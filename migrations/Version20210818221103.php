<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818221103 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE intent (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX unique_intent (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phrase (id INT AUTO_INCREMENT NOT NULL, intent_id INT NOT NULL, text VARCHAR(255) NOT NULL, INDEX IDX_A24BE60CC8E919CC (intent_id), UNIQUE INDEX unique_phrase (text, intent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE phrase ADD CONSTRAINT FK_A24BE60CC8E919CC FOREIGN KEY (intent_id) REFERENCES intent (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE phrase DROP FOREIGN KEY FK_A24BE60CC8E919CC');
        $this->addSql('DROP TABLE intent');
        $this->addSql('DROP TABLE phrase');
    }
}
