FROM php:8.0-cli AS cli

RUN apt-get update -y && \
    apt-get install wget git -y

RUN docker-php-ext-install mysqli pdo pdo_mysql bcmath

WORKDIR /app

FROM cli AS prod

COPY ./ /app
ENTRYPOINT ["php", "rocketbot"]

FROM cli AS dev

RUN wget https://get.symfony.com/cli/installer -O - | bash && \
    mv /root/.symfony/bin/symfony /usr/local/bin/symfony

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


RUN pecl install xdebug \
  && docker-php-ext-enable xdebug

RUN composer config -g cache-dir "/app/.composer-cache"

RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-install zip

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY ci/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini