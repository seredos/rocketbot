<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    App\BotHandlerBundle\BotHandlerBundle::class => ['all' => true],
    App\Api\RocketChatBundle\ApiRocketChatBundle::class => ['all' => true],
    App\Api\HolidayBundle\ApiHolidayBundle::class => ['all' => true],
    App\Api\TimeButlerBundle\ApiTimeButlerBundle::class => ['all' => true],
    \App\Api\DialogflowBundle\ApiDialogflowBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
];
