[![pipeline status](https://gitlab.com/seredos/rocketbot/badges/master/pipeline.svg)](https://gitlab.com/seredos/rocketbot/-/commits/master)
[![coverage report](https://gitlab.com/seredos/rocketbot/badges/master/coverage.svg)](https://gitlab.com/seredos/rocketbot/-/commits/master)

# rocket.bot

It is a simple bot that sends messages to a RocketChat server at certain times of the day.

## usage

### docker

The easiest way to use this bot is as a Docker image. The following arguments can be specified so that the program sends login/logout messages to a chat room accordingly

```bash
docker run --name my-own-mysql \
           -e MYSQL_ROOT_PASSWORD=mypass123 \
           -d mysql:8.0

docker run  -e ROCKETCHAT_URL="https://your.rocket.server" \
            -e ROCKETCHAT_USER="yourUserName" \
            -e ROCKETCHAT_PASSWORD="yourPassword" \
            -e ROCKETCHAT_ROOM="general" \
            -e DATABASE_URL="mysql://root:mypass123@database:3306/rocketbot?serverVersion=8.0" \
            --link my-own-mysql:database \
            --rm seredos/rocketbot:latest rocket:bot
```

If you want to use the intent publish or intent react handler, google cloud credentials are also required. (see https://cloud.google.com/docs/authentication/getting-started)

By default, the system is configured to send a message at 10 am and a message at 7 pm, on weekdays Monday to Friday.

[There](./ci/prod) is also a docker-compose available here for running the bot.

### composer installation

If composer is installed, the application can also be installed via composer:
```bash
export PATH="/home/[user]/.composer/vendor/bin:$PATH"
composer global require seredos/rocketbot
rocketbot rocket:bot
```

### manual installation (latest dev)

A tar.gz with the application can be downloaded [here](https://seredos.gitlab.io/rocketbot/download/rocket.bot.tar.gz). To run php8 is required!

Within the application there is an .env file in which the environment variables can be set.

```bash
...
ROCKETCHAT_URL=http://rocketchat:3000
ROCKETCHAT_USER=bot
ROCKETCHAT_PASSWORD=123
ROCKETCHAT_ROOM=general
```

Afterwards the bot can be started with the following command:
```bash
php rocketbot rocket:bot
```

## command line arguments

| name | description |
|------|-------------|
| interval | Interval in which the chat is checked/messages are sent (default 1 second) |

example:
```bash
docker run  -e ROCKETCHAT_URL="https://your.rocket.server" \
            -e ROCKETCHAT_USER="yourUserName" \
            -e ROCKETCHAT_PASSWORD="yourPassword" \
            -e ROCKETCHAT_ROOM="general" \
            --rm seredos/rocketbot:latest \
            rocket:bot \
            --interval=2 \
            -vvv
            
# or
php rocketbot rocket:bot --interval=2 -vvv
```

## configuration

The bot is configured through a series of handlers, which are executed repeatedly according to the specified interval. Which handlers are executed can be defined via the configuration file config/packages/bot_handler.yaml.

```bash
docker run  ... \
            -v your/bot_handler.yaml:/app/config/packages/bot_handler.yaml \
            --rm seredos/rocketbot:latest \
            rocket:bot
```

```yaml
# Default configuration for extension with alias: "bot_handler"
bot_handler:

    # the first executed handler name in list
    default_handler:      ~ # Required
    timezone: Europe/Berlin
    handlers:

        # Prototype
        name:

            # the handler type (see documentation)
            type:                 ~ # Required

            # the next called handler name
            next:                 ~
```
Depending on the specified type, further parameters result:

### group

The Group Handler is used to activate multiple handler lists. For this purpose, the handler executes all handlers that are specified with the corresponding parameter:
```yaml
bot_handler:
  default_handler: group_handler
  handlers:
    group_handler:
      type: group
      handlers:
        - handler1
        - handler2
    handler1:
      ...
    handler2:
      ...
```

### day_exclude

The handler is used to exclude certain days of the week. Thus, this handler executes the next handler only if the current day is not one of the specified weekdays

```yaml
bot_handler:
  default_handler: day_exclude_handler
  handlers:
    day_exclude_handler:
      type: day_exclude
      days: [Sunday, Saturday]
      next: handler1
    handler1:
      ...
```

### time_trigger

This handler executes the next handlers only at a certain time of day

```yaml
bot_handler:
  default_handler: time_triggger_handler
  handlers:
    time_triggger_handler:
      type: time_trigger
      time: 10:00
      next: handler1
    handler1:
      ...
```

### holiday_exclude

This handler executes the follow handler only if the current day is not a holiday.

```yaml
bot_handler:
  default_handler: holiday_exclude_handler
  handlers:
    holiday_exclude_handler:
      type: holiday_exclude
      countryCode: DE-BB
      next: handler1
    handler1:
      ...
```

### time_butler

This handler is used to trigger the following handlers only if a certain event is currently active within Timebutler or not.
The following example activates the following handlers only on days when there is no event with the corresponding filter.
```yaml
bot_handler:
  default_handler: time_butler_handler
  handlers:
    time_butler_handler:
      type: time_butler
      filter: '/[ \w]*, Homeoffice/m'
      invert: true
    handler1:
      ...
```
This handler also requires another environment variable that contains the path to the ics file on timebutler:
```bash
TIME_BUTLER_ICS=/path/[cryptname].ics
```

### interval

This handler always executes the following handlers in a certain interval
```yaml
bot_handler:
  default_handler: interval_handler
  handlers:
    interval_handler:
      type: interval
      interval: PT10S
      next: handler1
    handler1:
      ...
```

### send_message

This handler sends a specified message to a Rocket Chat room
```yaml
bot_handler:
  default_handler: message_handler
  handlers:
    message_handler:
      type: send_message
      room: general
      message: testmessage
```

### intent_store

This handler goes looks at the history of a room and stores all messages that contain reactions with the corresponding relation.

```yaml
bot_handler:
  default_handler: intent_handler
  handlers:
    intent_handler:
      type: intent_store
      room: general
      context: intent_context
```

### intent_publish

This handler takes the saved intents and sends them to the appropriate Dialogflow project

```yaml
bot_handler:
  default_handler: intent_handler
  handlers:
    intent_handler:
      type: intent_publish
      context: intent_context
```

### intent_react

This handler goes through the chat history of a room, sends the messages to the corresponding Dialogflow project and then outputs the corresponding response (if any)

```yaml
bot_handler:
  default_handler: intent_handler
  handlers:
    intent_handler:
      type: intent_react
      room: general
      context: intent_context
```

### bot_answer

This handler automatically sends a message, in a direct-channel. When a message arrives there. (e.g. vacation notification)

```yaml
bot_handler:
  default_handler: bot_answer_handler
  handlers:
    intent_handler:
      type: bot_answer
      message: vacation notification
```

## development environment

for xdebug to work (via docker) the variable HOSTIP must be set with the appropriate IP.
```bash
export HOSTIP=`ifconfig| grep 192.168 | grep 'inet ' | awk '{print $2}'`
```
If necessary, the gitlab registry must be registered for the use of the docker containers
```bash
docker login registry.gitlab.com
```

The development environment can then be started and used:
```bash
make up-rocket-chat
# or
make install-dev

make run-dev
$root@123:/app# rocketbot rocket:bot
```
To run phpstan use the following command:
```bash
make run-phpstan
```
To run phpdepend use the following command:
```bash
make run-phpdepend
```
To run phpmd use the following command:
```bash
make run-phpmd
```
To run the tests use the following command:
```bash
make run-tests
```

## latest reports

### coverage report: 
https://seredos.gitlab.io/rocketbot/coverage/

### phpdepend report:

![jdepend](https://seredos.gitlab.io/rocketbot/phpdepend/jdepend.svg)
![pyramid](https://seredos.gitlab.io/rocketbot/phpdepend/pyramid.svg)