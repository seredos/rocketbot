up-rocket-chat:
	docker-compose up -d rocketchat

install-db:
	docker-compose run --rm php bin/console doctrine:database:create --if-not-exists --no-interaction
	docker-compose run --rm php bin/console doctrine:migrations:migrate --no-interaction

install-dev: up-rocket-chat
	APP_ENV=dev docker-compose run --rm php composer install

install-prod:
	APP_ENV=prod docker-compose run --rm php composer install --no-dev --optimize-autoloader

run-dev:
	APP_ENV=dev \
	docker-compose run --rm php bash

run-deploy:
	docker-compose run --rm ssh /bin/ash -c '\
		rm -rf docker.tar.z && \
		cd ci/prod && \
		tar -czvf ../../docker.tar.gz . && \
		cd ../../ && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; rm -rf docker.tar.gz" && \
		sshpass -p ${SSH_PASSWORD} scp -o StrictHostKeyChecking=no docker.tar.gz ${SSH_USER}@${SSH_HOST}:${SSH_PATH} && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; tar -xf docker.tar.gz" && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; docker-compose stop && docker-compose rm -f rocketbot && docker-compose pull" && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; docker-compose up -d database phpmyadmin" && \
		sleep 10 && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; docker-compose run --rm rocketbot doctrine:database:create --if-not-exists --no-interaction" && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; docker-compose run --rm rocketbot doctrine:migrations:migrate --no-interaction" && \
		sshpass -p ${SSH_PASSWORD} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd ${SSH_PATH}; docker-compose up -d" \
	'

wait-rocketchat:
	docker-compose run --rm php dockerize -wait http://rocketchat:3000 -timeout 30s

run-tests: wait-rocketchat
	mkdir -p build/
	APP_ENV=dev \
	docker-compose run --rm php bash -c 'XDEBUG_MODE=coverage php vendor/bin/phpunit --coverage-text  --colors=never --coverage-html build/coverage/'

run-phpstan:
	docker-compose run --rm php php -dmemory_limit=256M vendor/bin/phpstan --configuration=phpstan.neon

run-phpdepend:
	mkdir -p build/phpdepend/
	docker-compose run --rm php vendor/bin/pdepend \
		--summary-xml=build/phpdepend/summary.xml \
		--jdepend-chart=build/phpdepend/jdepend.svg \
		--overview-pyramid=build/phpdepend/pyramid.svg \
		src/

run-phpmd:
	docker-compose run --rm php vendor/bin/phpmd src/ text codesize.xml

build-tar-gz:
	tar -czvf rocket.bot.tar.gz ./ \
		--exclude=rocket.bot.tar.gz \
		--exclude='./ci' \
		--exclude='./var' \
		--exclude='./.composer-cache' \
		--exclude='./.git' \
		--exclude='./build'

build-images:
	docker build --target dev -t registry.gitlab.com/seredos/rocketbot/dev:${IMAGE_TAG} .
	docker build -t registry.gitlab.com/seredos/rocketbot/mongo:${IMAGE_TAG} ./ci/mongo
	docker build -t registry.gitlab.com/seredos/rocketbot/rocket.chat:${IMAGE_TAG} ./ci/rocketchat
	docker build -t registry.gitlab.com/seredos/rocketbot/ssh:${IMAGE_TAG} ./ci/ssh

push-images: build-images
	docker push registry.gitlab.com/seredos/rocketbot/dev:${IMAGE_TAG}
	docker push registry.gitlab.com/seredos/rocketbot/mongo:${IMAGE_TAG}
	docker push registry.gitlab.com/seredos/rocketbot/rocket.chat:${IMAGE_TAG}
	docker push registry.gitlab.com/seredos/rocketbot/ssh:${IMAGE_TAG}

build-prod-image:
	rm -rf rocket.bot.tar.gz
	docker build --target prod -t seredos/rocketbot:${IMAGE_TAG} .
	docker tag seredos/rocketbot:${IMAGE_TAG} seredos/rocketbot:latest

push-prod-image: build-prod-image
	docker push seredos/rocketbot:${IMAGE_TAG}
	docker push seredos/rocketbot:latest