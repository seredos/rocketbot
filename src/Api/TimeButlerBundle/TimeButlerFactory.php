<?php

namespace App\Api\TimeButlerBundle;

use Psr\Log\LoggerInterface;

class TimeButlerFactory
{
    public function createTimeButlerApi(LoggerInterface $logger, string $url, string $ics): TimeButlerClient {
        $client = new TimeButlerClient($logger);
        $client->setUrl($url);
        $client->setIcs($ics);
        return $client;
    }
}