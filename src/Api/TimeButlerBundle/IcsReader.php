<?php

namespace App\Api\TimeButlerBundle;

class IcsReader
{
    /**
     * @param string $data
     * @return array<string,string|array<string,string>>
     * @throws TimeButlerException
     */
    public function load(string $data): array {
        $result = [];
        preg_match_all('/BEGIN:VCALENDAR[\n]([\w:.\n-\/]*)BEGIN:/m',
            $data,
            $matches,
            PREG_SET_ORDER,
            0);

        if(!isset($matches[0][1]))
            throw new TimeButlerException('ics file not readable!');
        
        preg_match_all('/([\w-]*):([\w\-.\/ ]*)/m', $matches[0][1],$matches, PREG_SET_ORDER, 0);
        foreach ($matches as $match) {
            $result[(string)$match[1]] = $match[2];
        }

        preg_match_all('/BEGIN:VEVENT\n([\s\S]*)\nEND:VEVENT/', $data, $matches, PREG_SET_ORDER, 0);
        $events = explode(PHP_EOL.'END:VEVENT'.PHP_EOL.'BEGIN:VEVENT'.PHP_EOL, $matches[0][1]);

        foreach ($events as $event) {
            $resultEvent = [];
            preg_match_all('/([\w\-;=]*):([\w\-.\/ ,\\\\:]*)/m', $event,$matches, PREG_SET_ORDER, 0);
            foreach ($matches as $match) {
                if(str_replace(';VALUE=', '',$match[1]) !== $match[1]) {
                    preg_match_all('/VALUE=(\w*)/m', $match[1], $valMatch, PREG_SET_ORDER, 0);
                    $resultEvent[explode(';',$match[1])[0]] = [
                        'type' => $valMatch[0][1],
                        'value' => $match[2]
                    ];
                    continue;
                }
                $resultEvent[(string)$match[1]] = str_replace('\\,',',',
                    (string)str_replace('\\n',PHP_EOL, $match[2]));
            }
            $result['VEVENT'][] = $resultEvent;
        }

        return $result;
    }
}