<?php

namespace App\Api\TimeButlerBundle\DependencyInjection;

use App\Api\TimeButlerBundle\TimeButlerClientInterface;
use App\Api\TimeButlerBundle\TimeButlerFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class ApiTimeButlerExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->register(TimeButlerFactory::class, TimeButlerFactory::class);

        $container->register(TimeButlerClientInterface::class, TimeButlerClientInterface::class)
            ->setFactory([new Reference(TimeButlerFactory::class), 'createTimeButlerApi'])
            ->addArgument(new Reference('monolog.logger.api_client'))
            ->addArgument($config['url'])
            ->addArgument($config['ics']);
    }
}