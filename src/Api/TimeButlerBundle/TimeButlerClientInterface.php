<?php

namespace App\Api\TimeButlerBundle;

use App\Api\Base\ClientInterface;

interface TimeButlerClientInterface extends ClientInterface
{
    /**
     * @param \DateTime $date
     * @param string $filter
     * @return array<int,array<string,mixed>>
     */
    public function getCalendar(\DateTime $date, string $filter): array;
}