<?php

namespace App\Api\TimeButlerBundle;

use App\Api\Base\Client;
use DateInterval;
use DateTime;
use Http\Client\Exception;

class TimeButlerClient extends Client implements TimeButlerClientInterface
{
    private string $ics;
    /**
     * @var array<string,mixed>|null
     */
    private ?array $cache = null;
    private DateTime $cacheLifetime;

    public function setIcs(string $ics): void
    {
        $this->ics = $ics;
    }

    /**
     * @param DateTime $date
     * @param string $filter
     * @return array<int,array<string,mixed>>
     * @throws TimeButlerException
     * @throws Exception
     * @throws \Exception
     */
    public function getCalendar(DateTime $date, string $filter): array
    {
        $useCache = false;
        $currentTimestamp = new DateTime();
        if (is_array($this->cache) && $this->cacheLifetime->getTimestamp() > $currentTimestamp->getTimestamp()) {
            $useCache = true;
        }

        if (!$useCache) {
            $response = $this->getHttpClient()->get($this->ics);
            $data = (new IcsReader())->load($response->getBody()->getContents());

            $this->cache = $data;
            if(is_string($data['CALENDAR-REFRESH-INTERVAL']))
                $this->cacheLifetime = $currentTimestamp->add(new DateInterval($data['CALENDAR-REFRESH-INTERVAL']));
        }

        return $this->generateResult($date, $filter);
    }

    /**
     * @param DateTime $date
     * @param string $filter
     * @return array<int,array<string,DateTime|string>>
     * @throws \Exception
     */
    private function generateResult(DateTime $date, string $filter): array {
        if(!is_array($this->cache) || !isset($this->cache['VEVENT']))
            throw new TimeButlerException('cache not readable!');

        $resultEvents = [];
        foreach ($this->cache['VEVENT'] as $event) {
            $startDate = new DateTime($event['DTSTART']['value']);
            $endDate = new DateTime($event['DTEND']['value']);
            if ($date->getTimestamp() > $startDate->getTimestamp()
                && $date->getTimestamp() < $endDate->getTimestamp()) {
                if (preg_match($filter, $event['SUMMARY'])) {
                    $resultEvents[] = [
                        'start' => $startDate,
                        'end' => $endDate,
                        'summary' => $event['SUMMARY']
                    ];
                }
            }
        }
        return $resultEvents;
    }
}