<?php

namespace App\Api\TimeButlerBundle\Tests;

use App\Api\TimeButlerBundle\IcsReader;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class IcsReaderTest extends TestCase
{
    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testReader(): void
    {
        $content = file_get_contents(__DIR__ . '/test.ics');
        
        $reader = new IcsReader();
        $data = $reader->load((string)$content);
        self::assertEquals(
            array(
            'VERSION' => '2.0',
            'PRODID' => '-//www.timebutler.de//iCal 4.0.3//EN',
            'METHOD' => 'PUBLISH',
            'CALSCALE' => 'GREGORIAN',
            'X-PUBLISHED-TTL' => 'PT4H',
            'CALENDAR-REFRESH-INTERVAL' => 'PT4H',
            'VEVENT' =>
                array(
                    0 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539307-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210104',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210105',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    1 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8553011-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210105',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210108',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 23:49 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    2 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539308-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210108',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210109',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    3 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539309-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210111',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210112',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    4 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8640163-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210112',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210115',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 12.1.2021, 11:00 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    5 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539310-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210115',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210116',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    6 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539311-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210118',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210119',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    7 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8707901-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210119',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210122',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 19.1.2021, 3:34 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    8 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539312-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210122',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210123',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    9 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539313-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210125',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210126',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    10 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8759043-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210126',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210129',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Sa, 23.1.2021, 19:33 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    11 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539314-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210129',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210130',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    12 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539315-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210201',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210202',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    13 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8759044-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210202',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210205',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Sa, 23.1.2021, 19:34 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    14 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539316-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210205',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210206',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    15 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539317-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210208',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210209',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    16 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8759045-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210209',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210212',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Sa, 23.1.2021, 19:34 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    17 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539318-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210212',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210213',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    18 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539319-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210215',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210216',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    19 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8994540-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210216',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210219',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 17.2.2021, 17:58 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    20 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539320-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210219',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210220',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    21 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539321-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210222',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210223',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    22 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8994542-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210223',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210226',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 17.2.2021, 17:58 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    23 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539322-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210226',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210227',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    24 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539323-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210301',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210302',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    25 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9107911-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210302',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210305',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 3.3.2021, 9:42 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    26 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539324-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210305',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210306',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    27 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539325-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210308',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210309',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    28 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9107916-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210309',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210312',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 3.3.2021, 9:42 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    29 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539326-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210312',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210313',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    30 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539327-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210315',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210316',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    31 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9107921-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210316',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210319',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 3.3.2021, 9:42 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    32 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539328-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210319',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210320',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    33 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539329-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210322',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210323',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    34 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9107928-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210323',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210326',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Mi, 3.3.2021, 9:42 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    35 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539330-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210326',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210327',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    36 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539331-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210329',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210330',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    37 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9320192-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210330',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210403',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 30.3.2021, 10:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    38 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9350977-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210406',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210409',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 6.4.2021, 10:25 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    39 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539332-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210409',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210410',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    40 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539333-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210412',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210413',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    41 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9384170-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210413',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210416',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: So, 11.4.2021, 18:39 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    42 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539334-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210416',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210417',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    43 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539335-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210419',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210420',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    44 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9384171-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210420',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210423',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: So, 11.4.2021, 18:40 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    45 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539336-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210423',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210424',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    46 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539337-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210426',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210427',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    47 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9384172-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210427',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210430',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: So, 11.4.2021, 18:40 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    48 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539338-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210430',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210501',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    49 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539339-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210503',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210504',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    50 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9566171-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210504',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210507',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 4.5.2021, 14:50 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    51 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539340-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210507',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210508',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    52 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539341-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210510',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210511',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    53 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9566176-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210511',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210514',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 4.5.2021, 14:51 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    54 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539342-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210514',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210515',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    55 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539343-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210517',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210518',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    56 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9566188-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210518',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210521',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 4.5.2021, 14:51 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    57 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539344-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210521',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210522',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    58 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9566183-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210525',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210528',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 4.5.2021, 14:51 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    59 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539345-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210528',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210529',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    60 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9621829-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210531',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210612',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 10,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 11.5.2021, 11:31 Uhr
Genehmigt am: Di, 11.5.2021, 12:58 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    61 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539350-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210614',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210615',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    62 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9871707-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210615',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210618',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 15.6.2021, 10:24 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    63 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539351-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210618',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210619',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    64 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539352-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210621',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210622',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    65 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9921303-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210622',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210625',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 22.6.2021, 3:14 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    66 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539353-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210625',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210626',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    67 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-8539354-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210628',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210629',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 4.1.2021, 11:02 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    68 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9971792-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210629',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210702',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 29.6.2021, 0:58 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    69 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10002888-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210702',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210703',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Fr, 2.7.2021, 8:42 Uhr
Genehmigt am: Fr, 2.7.2021, 13:35 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    70 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10016562-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210705',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210706',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 5.7.2021, 9:51 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    71 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10047927-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210708',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210709',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Do, 8.7.2021, 9:58 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    72 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10133273-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210712',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210717',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 5,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 20.7.2021, 23:54 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    73 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10133274-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210719',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210721',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 20.7.2021, 23:55 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    74 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10133275-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210721',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210722',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 20.7.2021, 23:56 Uhr
Genehmigt am: Fr, 23.7.2021, 17:10 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    75 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10167340-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210726',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210728',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 26.7.2021, 15:44 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    76 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10205029-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210802',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210804',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 2.8.2021, 1:59 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    77 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10225536-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210804',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210805',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 3.8.2021, 16:21 Uhr
Genehmigt am: Di, 3.8.2021, 16:22 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    78 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10255777-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210809',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210811',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 9.8.2021, 9:56 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    79 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10266224-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210811',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210812',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 1,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 10.8.2021, 10:38 Uhr
Genehmigt am: Di, 10.8.2021, 14:09 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    80 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10282042-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210812',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210814',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Do, 12.8.2021, 10:10 Uhr
Genehmigt am: Do, 12.8.2021, 11:48 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    81 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-10295419-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20210816',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20210818',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Homeoffice',
                            'DESCRIPTION' => 'Homeoffice


Status: Bearbeitung abgeschlossen
Arbeitstage: 2,0

Eingetragen von: Arne von Appen
Eingegeben am: Mo, 16.8.2021, 3:15 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    82 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9623646-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20211220',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20211224',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 4,0

Eingetragen von: Arne von Appen
Eingegeben am: Di, 11.5.2021, 13:22 Uhr
Genehmigt am: Di, 11.5.2021, 13:39 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                    83 =>
                        array(
                            'CREATED' => '20210820T232706',
                            'UID' => 'ical-9834054-timebutler-de',
                            'DTSTART' =>
                                array(
                                    'value' => '20211228',
                                    'type' => 'DATE',
                                ),
                            'DTEND' =>
                                array(
                                    'value' => '20211231',
                                    'type' => 'DATE',
                                ),
                            'TRANSP' => 'TRANSPARENT',
                            'SUMMARY' => 'Arne von Appen, Jahresurlaub',
                            'DESCRIPTION' => 'Jahresurlaub


Status: Genehmigt
Arbeitstage: 3,0

Eingetragen von: Arne von Appen
Eingegeben am: Do, 10.6.2021, 8:20 Uhr
Genehmigt am: Do, 10.6.2021, 8:20 Uhr


Powered by timebutler.de',
                            'CATEGORIES' => 'timebutler.de',
                            'DTSTAMP' => '20210820T232706',
                        ),
                ),
        ), $data);
    }
}