<?php

namespace App\Api\HolidayBundle;

use App\Api\Base\Client;

class HolidayClient extends Client implements HolidayClientInterface
{
    public function api(): HolidayApi {
        return new HolidayApi($this);
    }
}