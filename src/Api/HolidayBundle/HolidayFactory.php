<?php

namespace App\Api\HolidayBundle;

use Psr\Log\LoggerInterface;

class HolidayFactory
{
    public function createHolidayApi(LoggerInterface $logger, string $url): HolidayClientInterface {
        $holidayClient = new HolidayClient($logger);
        $holidayClient->setUrl($url);
        return $holidayClient;
    }
}