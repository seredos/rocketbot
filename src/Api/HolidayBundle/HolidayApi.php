<?php

namespace App\Api\HolidayBundle;

use App\Api\Base\AbstractApi;
use Http\Client\Exception;

class HolidayApi extends AbstractApi
{
    /**
     * @param string $countryCode1
     * @param string $countryCode2
     * @param int $offset
     * @return bool
     * @throws Exception
     * @SuppressWarnings("static")
     */
    public function isHoliday(string $countryCode1, string $countryCode2, int $offset): bool {
        $response = $this->client->getHttpClient()
            ->get(AbstractApi::prepareUri('/api/v3/IsTodayPublicHoliday/'.$countryCode1,
                ['countryCode' => $countryCode2, 'offset' => (string)$offset]), []);
        return $response->getStatusCode() === 200;
    }
}