<?php

namespace App\Api\HolidayBundle;

interface HolidayClientInterface
{
    public function api(): HolidayApi;
}