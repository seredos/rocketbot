<?php

namespace App\Api\HolidayBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('api_holiday');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('url')->end()
            ->end();

        return $treeBuilder;
    }
}