<?php

namespace App\Api\HolidayBundle\DependencyInjection;

use App\Api\HolidayBundle\HolidayClientInterface;
use App\Api\HolidayBundle\HolidayFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class ApiHolidayExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->register(HolidayFactory::class, HolidayFactory::class);

        $container->register(HolidayClientInterface::class, HolidayClientInterface::class)
            ->setFactory([new Reference(HolidayFactory::class), 'createHolidayApi'])
            ->addArgument(new Reference('monolog.logger.api_client'))
            ->addArgument($config['url']);
    }
}