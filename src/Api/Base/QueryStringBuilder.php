<?php

namespace App\Api\Base;

use function array_keys;
use function array_map;
use function count;
use function implode;
use function is_array;
use function range;
use function rawurlencode;
use function sprintf;

class QueryStringBuilder
{
    /**
     * @param array<string,string|mixed> $query
     * @return string
     */
    public static function build(array $query): string
    {
        return sprintf('?%s', implode('&', array_map(function ($value, $key): string {
            return self::encode($value, $key);
        }, $query, array_keys($query))));
    }

    /**
     * @param string|array<string,mixed> $query
     * @param string $prefix
     * @return string
     */
    private static function encode($query, string $prefix): string
    {
        if (!is_array($query)) {
            return self::rawUrlEncode($prefix).'='.self::rawUrlEncode($query);
        }

        $isList = self::isList($query);

        return implode('&', array_map(function ($value, $key) use ($prefix, $isList): string {
            $prefix = $isList ? $prefix.'[]' : $prefix.'['.$key.']';

            return self::encode($value, $prefix);
        }, $query, array_keys($query)));
    }

    /**
     * @param array<string|int,string> $query
     * @return bool
     */
    private static function isList(array $query): bool
    {
        if (!isset($query[0])) {
            return false;
        }

        return array_keys($query) === range(0, count($query) - 1);
    }

    private static function rawUrlEncode(?string $value): string
    {
        if (null === $value) {
            return '0';
        }

        return rawurlencode((string) $value);
    }
}