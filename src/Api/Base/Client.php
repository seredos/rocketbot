<?php

namespace App\Api\Base;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Psr\Log\LoggerInterface;

abstract class Client implements ClientInterface
{
    protected Builder $builder;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, Builder $builder = null) {
        $this->builder = $builder ?? new Builder();
        $this->logger = $logger;
    }

    public function setUrl(string $url): void {
        $uri = $this->builder->getUriFactory()->createUri($url);

        $this->builder->removePlugin(AddHostPlugin::class);
        $this->builder->addPlugin(new AddHostPlugin($uri));
    }

    public function getHttpClient(): HttpMethodsClientInterface {
        return $this->builder->getHttpClient();
    }
    
    public function getLogger(): LoggerInterface {
        return $this->logger;
    }
}