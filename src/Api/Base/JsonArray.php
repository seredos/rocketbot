<?php

namespace App\Api\Base;

use RuntimeException;
use function get_debug_type;
use function is_array;
use function json_decode;
use function json_encode;
use function json_last_error;
use function json_last_error_msg;
use function sprintf;
use const JSON_ERROR_NONE;

class JsonArray
{
    /**
     * @param string $json
     * @return array<string,mixed>
     */
    public static function decode(string $json): array
    {
        /** @var scalar|array<string,mixed>|null */
        $data = json_decode($json, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException(sprintf('json_decode error: %s', json_last_error_msg()));
        }

        if (!is_array($data)) {
            throw new RuntimeException(sprintf('json_decode error: Expected JSON of type array, %s given.', get_debug_type($data)));
        }

        return $data;
    }

    /**
     * @param array<string,mixed> $value
     * @return string
     */
    public static function encode(array $value): string
    {
        $json = json_encode($value);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException(sprintf('json_encode error: %s', json_last_error_msg()));
        }

        /** @var string */
        return $json;
    }
}