<?php

namespace App\Api\Base;

use Http\Client\Common\HttpMethodsClientInterface;
use Psr\Log\LoggerInterface;

interface ClientInterface
{
    public function getHttpClient(): HttpMethodsClientInterface;
    
    public function getLogger(): LoggerInterface;
}