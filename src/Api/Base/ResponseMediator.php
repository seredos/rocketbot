<?php

namespace App\Api\Base;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use function array_shift;
use function in_array;

class ResponseMediator
{
    public const CONTENT_TYPE_HEADER = 'Content-Type';
    public const JSON_CONTENT_TYPE = 'application/json';

    /**
     * @param ResponseInterface $response
     * @return array<mixed>
     * @SuppressWarnings("static")
     */
    public static function getContent(ResponseInterface $response): array
    {
        $body = (string) $response->getBody();

        if (!in_array($body, ['', 'null', 'true', 'false'], true)
            && \str_starts_with(
                $response->getHeaderLine(self::CONTENT_TYPE_HEADER), self::JSON_CONTENT_TYPE)) {
            return JsonArray::decode($body);
        }

        throw new NotFoundHttpException('content type not supported! - '. $body);
    }
}