<?php

namespace App\Api\Base;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin;
use Http\Client\Common\PluginClientFactory;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

class Builder
{
    private ClientInterface $client;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private UriFactoryInterface $uriFactory;
    private ?HttpMethodsClientInterface $pluginClient;
    /**
     * @var Plugin[]
     */
    private array $plugins = [];

    /**
     * @param ClientInterface|null $client
     * @param RequestFactoryInterface|null $requestFactory
     * @param StreamFactoryInterface|null $streamFactory
     * @param UriFactoryInterface|null $uriFactory
     * @SuppressWarnings("static")
     */
    public function __construct(ClientInterface $client = null,
                                RequestFactoryInterface $requestFactory = null,
                                StreamFactoryInterface $streamFactory = null,
                                UriFactoryInterface $uriFactory = null)
    {
        $this->client = $client ?? Psr18ClientDiscovery::find();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
        $this->uriFactory = $uriFactory ?? Psr17FactoryDiscovery::findUriFactory();
        $this->pluginClient = null;
    }

    public function getHttpClient(): HttpMethodsClientInterface {
        if($this->pluginClient === null) {
            $this->pluginClient = new HttpMethodsClient((new PluginClientFactory())->createClient($this->client, $this->plugins), $this->requestFactory, $this->streamFactory);
        }

        return $this->pluginClient;
    }

    public function getUriFactory(): UriFactoryInterface {
        return $this->uriFactory;
    }

    public function addPlugin(Plugin $plugin): void {
        $this->plugins[] = $plugin;
        $this->pluginClient = null;
    }

    public function removePlugin(string $pluginClass): void
    {
        foreach ($this->plugins as $idx => $plugin) {
            if ($plugin instanceof $pluginClass) {
                unset($this->plugins[$idx]);
                $this->pluginClient = null;
            }
        }
    }
}