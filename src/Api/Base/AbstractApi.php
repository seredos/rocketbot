<?php

namespace App\Api\Base;

use Http\Client\Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use function array_filter;
use function array_merge;
use function count;
use function sprintf;

class AbstractApi
{
    protected ClientInterface $client;

    public function __construct(ClientInterface $client) {
        $this->client = $client;
    }

    /**
     * @param array<string,string> $params
     * @param array<string,string> $headers
     * @throws Exception
     * @return array<mixed>
     * @SuppressWarnings("static")
     */
    protected function get(string $uri, array $params = [], array $headers = []): array
    {
        $response = $this->client->getHttpClient()->get(self::prepareUri($uri, $params), $headers);

        switch ($response->getStatusCode()) {
            case 401:
                throw new UnauthorizedHttpException('Unauthorized');
            case 502:
                $this->client->getLogger()->error('bad gateway', ['uri' => $uri, 'params' => $params]);
                return [];
            default:
                return ResponseMediator::getContent($response);
        }
    }
    
    /**
     * @param array<string,string|true> $params
     * @param array<string,string> $headers
     * @throws Exception
     * @return array<mixed>
     * @SuppressWarnings("static")
     */
    protected function post(string $uri, array $params = [], array $headers = []): array
    {
        $body = self::prepareJsonBody($params);

        if (null !== $body) {
            $headers = self::addJsonContentType($headers);
        }

        $response = $this->client->getHttpClient()->post(self::prepareUri($uri), $headers, $body);

        switch ($response->getStatusCode()) {
            case 401:
                throw new UnauthorizedHttpException('Unauthorized');
            case 502:
                $this->client->getLogger()->error('bad gateway', ['uri' => $uri, 'params' => $params]);
                return [];
            default:
                return ResponseMediator::getContent($response);
        }
    }

    /**
     * @param array<string,string> $headers
     * @return array<mixed>
     */
    private static function addJsonContentType(array $headers): array
    {
        return array_merge([ResponseMediator::CONTENT_TYPE_HEADER => ResponseMediator::JSON_CONTENT_TYPE], $headers);
    }

    /**
     * @param string $uri
     * @param array<string,string> $query
     * @return string
     * @SuppressWarnings("static")
     */
    protected static function prepareUri(string $uri, array $query = []): string
    {
        $query = array_filter($query, function ($value): bool {
            return null !== $value;
        });

        return sprintf('%s%s', $uri, QueryStringBuilder::build($query));
    }

    /**
     * @param array<string,string|true> $params
     * @return string|null
     * @SuppressWarnings("static")
     */
    private static function prepareJsonBody(array $params): ?string
    {
        $params = array_filter($params, function ($value): bool {
            return null !== $value;
        });

        if (0 === count($params)) {
            return null;
        }

        return JsonArray::encode($params);
    }
}