<?php

namespace App\Api\DialogflowBundle\Entity;

use App\Api\DialogflowBundle\Repository\IntentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IntentRepository::class)
 * @ORM\Table(uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_intent", columns={"name"})
 * })
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Intent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\OneToMany(targetEntity=Phrase::class, mappedBy="intent", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $phrases;

    /**
     * @ORM\OneToMany(targetEntity=Context::class, mappedBy="intent", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $contexts;

    public function __construct()
    {
        $this->phrases = new ArrayCollection();
        $this->contexts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Phrase[]
     */
    public function getPhrases(): Collection
    {
        return $this->phrases;
    }

    public function addPhrase(Phrase $phrase): self
    {
        if (!$this->phrases->contains($phrase)) {
            $this->phrases[] = $phrase;
            $phrase->setIntent($this);
        }

        return $this;
    }

    public function removePhrase(Phrase $phrase): self
    {
        if ($this->phrases->removeElement($phrase)) {
            // set the owning side to null (unless already changed)
            if ($phrase->getIntent() === $this) {
                $phrase->setIntent(null);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getContexts(): ArrayCollection|Collection
    {
        return $this->contexts;
    }

    public function addContext(Context $context): self
    {
        if (!$this->contexts->contains($context)) {
            $this->contexts[] = $context;
            $context->setIntent($this);
        }
        return $this;
    }

    public function removeContext(Context $context): self
    {
        if ($this->contexts->removeElement($context)) {
            if ($context->getIntent() === $this)
                $context->setIntent(null);
        }
        return $this;
    }
}
