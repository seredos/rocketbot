<?php

namespace App\Api\DialogflowBundle\Entity;

use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Repository\PhraseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhraseRepository::class)
 * @ORM\Table(uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_phrase", columns={"text","intent_id"})
 * })
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Phrase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $text;

    /**
     * @ORM\ManyToOne(targetEntity=Intent::class, inversedBy="phrases")
     * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
     */
    private ?Intent $intent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getIntent(): ?Intent
    {
        return $this->intent;
    }

    public function setIntent(?Intent $intent): self
    {
        $this->intent = $intent;

        return $this;
    }
}
