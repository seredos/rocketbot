<?php

namespace App\Api\DialogflowBundle\Entity;

use App\Api\DialogflowBundle\Repository\ContextRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContextRepository::class)
 * @ORM\Table(uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_context", columns={"name","intent_id"})
 * })
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Context
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private ?string $name = null;
    /**
     * @ORM\ManyToOne(targetEntity=Intent::class, inversedBy="contexts")
     * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
     */
    private ?Intent $intent;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Context
     */
    public function setName(?string $name): Context
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Intent|null
     */
    public function getIntent(): ?Intent
    {
        return $this->intent;
    }

    /**
     * @param Intent|null $intent
     * @return Context
     */
    public function setIntent(?Intent $intent): Context
    {
        $this->intent = $intent;
        return $this;
    }
}
