<?php

namespace App\Api\DialogflowBundle\DependencyInjection;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Repository\ContextRepository;
use App\Api\DialogflowBundle\Repository\IntentRepository;
use App\Api\DialogflowBundle\Repository\PhraseRepository;
use App\Api\DialogflowBundle\Service\IntentDbService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class ApiDialogflowExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->register(DialogflowClient::class, DialogflowClient::class)
            ->addArgument($config['project'])
            ->addArgument(new Reference('monolog.logger.dialogflow'))
            ->setPublic(true);

        $container->autowire(IntentRepository::class,IntentRepository::class);
        $container->autowire(PhraseRepository::class,PhraseRepository::class);
        $container->autowire(ContextRepository::class, ContextRepository::class);
        
        $container->autowire(IntentDbService::class, IntentDbService::class)
            ->setPublic(true);
    }
}