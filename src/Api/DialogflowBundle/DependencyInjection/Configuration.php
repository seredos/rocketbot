<?php

namespace App\Api\DialogflowBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('api_dialogflow');

        $treeBuilder->getRootNode()
            ->children()
            ->scalarNode('project')->end()
            ->end();
        return $treeBuilder;
    }
}