<?php

namespace App\Api\DialogflowBundle\Repository;

use App\Api\DialogflowBundle\Entity\Context;
use App\Api\DialogflowBundle\Entity\Intent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Intent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Intent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Intent[]    findAll()
 * @method Intent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Intent::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function save(Intent $intent): void {
        $this->_em->persist($intent);
        $this->_em->flush();
    }

    /**
     * @param array<string> $contexts
     * @return Intent[]
     */
    public function findByContexts(array $contexts): array {
        $query = $this->createQueryBuilder('i')
            ->innerJoin(Context::class,'c')
            ->where('c.name IN(:names)')
            ->getQuery();
        
        return $query->setParameter('names', $contexts)->getResult();
    }
    
    // /**
    //  * @return Intent[] Returns an array of Intent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Intent
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
