<?php

namespace App\Api\DialogflowBundle\Api;

use App\Api\DialogflowBundle\Entity\Intent;
use Google\ApiCore\ApiException;

class IntentApi
{
    private IntentStrategy $intentStrategy;
    public function __construct(IntentStrategy $intentStrategy) {
        $this->intentStrategy = $intentStrategy;
    }

    /**
     * @param array<Intent> $intents
     * @throws ApiException
     */
    public function delete(array $intents): void {
        $this->intentStrategy->delete($intents);
    }

    /**
     * @param Intent $intent
     * @throws ApiException
     */
    public function create(Intent $intent): void {
        $this->intentStrategy->create($intent);
    }

    /**
     * @param string $message
     * @param string|null $context
     * @return Intent|null
     * @throws ApiException
     */
    public function detect(string $message, ?string $context = null): ?Intent {
        return $this->intentStrategy->detect($message, $context);
    }
}