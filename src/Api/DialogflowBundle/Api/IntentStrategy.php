<?php

namespace App\Api\DialogflowBundle\Api;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Service\GoogleIntentManager;
use Google\ApiCore\ApiException;
use Google\Cloud\Dialogflow\V2\Context;
use Google\Cloud\Dialogflow\V2\ContextsClient;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Google\Cloud\Dialogflow\V2\QueryParameters;
use Google\Cloud\Dialogflow\V2\TextInput;

class IntentStrategy
{
    private DialogflowClient $client;
    private GoogleIntentManager $googleIntentManager;

    public function __construct(DialogflowClient $client, GoogleIntentManager $googleIntentManager)
    {
        $this->client = $client;
        $this->googleIntentManager = $googleIntentManager;
    }

    /**
     * @param array<Intent> $intents
     * @throws ApiException
     */
    public function delete(array $intents): void {
        $this->googleIntentManager->delete($intents);
    }

    /**
     * @param Intent $intent
     * @throws ApiException
     */
    public function create(Intent $intent): void {
        $this->googleIntentManager->create($intent);
    }

    /**
     * @param string $message
     * @param string|null $context
     * @return Intent|null
     * @throws ApiException
     * @SuppressWarnings("static")
     */
    public function detect(string $message, ?string $context = null): ?Intent {
        $textInput = new TextInput();
        $textInput->setText($message);
        $textInput->setLanguageCode('de');

        $queryInput = new QueryInput();
        $queryInput->setText($textInput);

        $queryParams = new QueryParameters();
        if($context) {
            $contextInput = new Context();
            $contextInput->setLifespanCount(1);
            $contextName = ContextsClient::contextName($this->client->getProject(), $this->client->getSessionId(), $context);
            $contextInput->setName($contextName);
            $queryParams->setContexts(array($contextInput));
        }

        $session = $this->client->getSession();
        $response = $this->client->getSessionClient()->detectIntent($session, $queryInput, ['queryParams' => $queryParams]);
        $intent = $response->getQueryResult()?->getIntent();
        if(!$intent)
            return null;

        $result = new Intent();
        $result->setName($intent->getDisplayName());
        return $result;
    }
}