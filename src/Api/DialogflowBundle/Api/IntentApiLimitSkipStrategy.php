<?php

namespace App\Api\DialogflowBundle\Api;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Service\GoogleIntentManager;
use Google\ApiCore\ApiException;
use GuzzleHttp\Exception\ConnectException;
use Psr\Log\LoggerInterface;

class IntentApiLimitSkipStrategy extends IntentStrategy
{
    private LoggerInterface $logger;
    public function __construct(DialogflowClient $client, GoogleIntentManager $googleIntentManager, LoggerInterface $logger)
    {
        parent::__construct($client, $googleIntentManager);
        $this->logger = $logger;
    }

    public function delete(array $intents): void
    {
        try {
            parent::delete($intents);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit. skip...');
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
        }
    }

    public function create(Intent $intent): void
    {
        try {
            parent::create($intent);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit. skip...');
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
        }
    }

    public function detect(string $message, ?string $context = null): ?Intent
    {
        try {
            return parent::detect($message, $context);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit. skip...');
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
        }
        return null;
    }
}