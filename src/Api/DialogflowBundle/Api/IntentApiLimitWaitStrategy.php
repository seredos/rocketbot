<?php

namespace App\Api\DialogflowBundle\Api;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Service\GoogleIntentManager;
use Google\ApiCore\ApiException;
use GuzzleHttp\Exception\ConnectException;
use Psr\Log\LoggerInterface;

class IntentApiLimitWaitStrategy extends IntentStrategy
{
    private LoggerInterface $logger;
    public function __construct(DialogflowClient $client, GoogleIntentManager $googleIntentManager, LoggerInterface $logger)
    {
        $this->logger = $logger;
        parent::__construct($client, $googleIntentManager);
    }

    public function delete(array $intents): void
    {
        try {
            parent::delete($intents);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit! wait 60 seconds...');
            sleep(60);
            $this->delete($intents);
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
            sleep(60);
            $this->delete($intents);
        }
    }

    public function create(Intent $intent): void
    {
        try {
            parent::create($intent);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit! wait 60 seconds...');
            sleep(60);
            $this->create($intent);
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
            sleep(60);
            $this->create($intent);
        }
    }

    public function detect(string $message, ?string $context = null): ?Intent
    {
        try {
            return parent::detect($message, $context);
        }catch (ApiException $e) {
            if ($e->getCode() != 8)
                throw $e;
            $this->logger->warning('api rate limit! wait 60 seconds...');
            sleep(60);
            $this->detect($message, $context);
        } catch (ConnectException) {
            $this->logger->warning('can\'t connect to host');
            sleep(60);
            $this->detect($message, $context);
        }

        return null;
    }
}