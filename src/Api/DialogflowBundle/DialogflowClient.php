<?php

namespace App\Api\DialogflowBundle;

use App\Api\DialogflowBundle\Api\IntentApi;
use App\Api\DialogflowBundle\Api\IntentApiLimitSkipStrategy;
use App\Api\DialogflowBundle\Api\IntentApiLimitWaitStrategy;
use App\Api\DialogflowBundle\Service\GoogleIntentManager;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Psr\Log\LoggerInterface;

class DialogflowClient
{
    public const TYPE_WAIT = 'wait';
    public const TYPE_SKIP = 'skip';
    private SessionsClient $sessionsClient;
    private string $sessionId;
    private string $project;
    private string $session;
    private LoggerInterface $logger;

    public function __construct(string $project, LoggerInterface $logger) {
        $this->sessionsClient = new SessionsClient();
        $this->sessionId = uniqid();
        $this->project = $project;
        $this->logger = $logger;
        $this->session = $this->sessionsClient->sessionName($project, $this->sessionId);
    }

    public function intents(string $type = self::TYPE_SKIP): IntentApi {
        $manager = new GoogleIntentManager($this);
        $strategy = match ($type) {
            self::TYPE_WAIT => new IntentApiLimitWaitStrategy($this, $manager, $this->logger),
            default => new IntentApiLimitSkipStrategy($this, $manager, $this->logger),
        };
        
        return new IntentApi($strategy);
    }
    
    public function getSession(): string {
        return $this->session;
    }
    
    public function getSessionId(): string {
        return $this->sessionId;
    }
    
    public function getSessionClient(): SessionsClient {
        return $this->sessionsClient;
    }
    
    public function getProject(): string {
        return $this->project;
    }
}