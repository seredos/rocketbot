<?php

namespace App\Api\DialogflowBundle\Service;

use App\Api\DialogflowBundle\Entity\Context;
use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Entity\Phrase;
use App\Api\DialogflowBundle\Repository\IntentRepository;
use App\Api\DialogflowBundle\Repository\PhraseRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

class IntentDbService
{
    private IntentRepository $intentRepository;
    private PhraseRepository $phraseRepository;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface  $intentServiceLogger,
                                IntentRepository $intentRepository,
                                PhraseRepository $phraseRepository) {
        $this->intentRepository = $intentRepository;
        $this->phraseRepository = $phraseRepository;
        $this->logger = $intentServiceLogger;
    }

    /**
     * @param array<string> $contexts
     * @return Intent[]
     */
    public function findIntents(array $contexts = []): array
    {
        if(!count($contexts))
            return $this->intentRepository->findAll();

        return $this->intentRepository->findByContexts($contexts);
    }

    /**
     * @param Intent $intent
     * @param array<string> $contexts
     */
    private function addContexts(Intent $intent, array $contexts = []): void {
        foreach($intent->getContexts() as $context) {
            $key = array_search($context->getName(), $contexts);
            if ($key !== false) {
                unset($contexts[$key]);
            }
        }

        foreach($contexts as $context) {
            $dbContext = new Context();
            $dbContext->setName($context);
            $intent->addContext($dbContext);
        }
    }

    /**
     * @param string $name
     * @param array<string> $messages
     * @param array<string> $contexts
     */
    public function saveIntent(string $name, array $messages, array $contexts = []): void {
        $intent = $this->intentRepository->findOneBy(['name' => $name]);
        if(!$intent) {
            $intent = new Intent();
            $intent->setName($name);
        }
        
        $this->addContexts($intent, $contexts);

        foreach($messages as $message) {
            $phrase = null;
            if($intent->getId())
                $phrase = $this->phraseRepository->findOneBy(['intent' => $intent, 'text' => $message]);

            if(!$phrase && strlen($message) < 255) {
                $phrase = new Phrase();
                $phrase->setText($message);
                $phrase->setIntent($intent);
                $intent->addPhrase($phrase);
            }
        }

        try {
            $this->intentRepository->save($intent);
        } catch (OptimisticLockException|ORMException $e) {
            $this->logger->warning('cant save message', ['exception' => $e,
                'messages' => $messages,
                'reaction' => $name]);
        }
    }
}