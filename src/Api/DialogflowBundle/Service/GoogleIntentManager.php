<?php

namespace App\Api\DialogflowBundle\Service;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Entity\Intent;
use Google\ApiCore\ApiException;
use Google\Cloud\Dialogflow\V2\ContextsClient;
use Google\Cloud\Dialogflow\V2\Intent as GoogleIntent;
use Google\Cloud\Dialogflow\V2\Intent\Message;
use Google\Cloud\Dialogflow\V2\Intent\Message\Text;
use Google\Cloud\Dialogflow\V2\Intent\TrainingPhrase;
use Google\Cloud\Dialogflow\V2\Intent\TrainingPhrase\Part;
use Google\Cloud\Dialogflow\V2\IntentsClient;

class GoogleIntentManager
{
    private IntentsClient $intentsClient;
    private DialogflowClient $client;
    public function __construct(DialogflowClient $client)
    {
        $this->intentsClient = new IntentsClient();
        $this->client = $client;
    }

    /**
     * @param array<Intent> $intents
     * @throws ApiException
     */
    public function delete(array $intents): void {
        $parent = $this->intentsClient->agentName($this->client->getProject());
        $googleIntents = $this->intentsClient->listIntents($parent);

        $strIntents = array_map(function (Intent $value) { return $value->getName();}, $intents);

        foreach ($googleIntents as $googleIntent) {
            if(in_array($googleIntent->getDisplayName(), $strIntents)) {
                $this->intentsClient->deleteIntent($googleIntent->getName());
            }
        }
    }

    /**
     * @param Intent $intent
     * @throws ApiException
     * @SuppressWarnings("static")
     */
    public function create(Intent $intent): void {
        $intentName = $intent->getName();
        if(!$intentName)
            return;

        $parent = $this->intentsClient->agentName($this->client->getProject());

        $trainingPhrases = [];
        foreach ($intent->getPhrases() as $phrase) {
            $text = $phrase->getText();

            if($text) {
                $part = (new Part())
                    ->setText($text);
                $trainingPhrase = (new TrainingPhrase())
                    ->setParts([$part]);
                $trainingPhrases[] = $trainingPhrase;
            }
        }

        $text = (new Text())
            ->setText([$intentName]);
        $message = (new Message())
            ->setText($text);

        $googleIntent = (new GoogleIntent())
            ->setDisplayName($intentName)
            ->setTrainingPhrases(
                $trainingPhrases
            )
            ->setMessages([$message]);

        $contexts = [];
        foreach ($intent->getContexts() as $context) {
            $contexts[] = ContextsClient::contextName($this->client->getProject(), $this->client->getSessionId(), $context->getName());
        }

        $googleIntent->setInputContextNames($contexts);
        $this->intentsClient->createIntent($parent, $googleIntent);
    }
}