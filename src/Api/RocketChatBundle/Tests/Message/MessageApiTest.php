<?php

namespace App\Api\RocketChatBundle\Tests\Message;

use App\Api\RocketChatBundle\Message\MessageApi;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use App\Api\RocketChatBundle\User\Token;
use App\Api\RocketChatBundle\User\User;
use Http\Client\Common\HttpMethodsClientInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class MessageApiTest extends TestCase
{
    private MockObject $client;
    private MockObject $rocketChatClient;
    private MessageApi $api;
    protected function setUp(): void
    {
        parent::setUp();
        $this->rocketChatClient = $this->getMockBuilder(RocketChatClientInterface::class)->getMock();
        $this->client = $this->getMockBuilder(HttpMethodsClientInterface::class)->getMock();

        $this->rocketChatClient->expects(self::any())
            ->method('getHttpClient')
            ->willReturn($this->client);

        $this->api = new MessageApi($this->rocketChatClient);
    }

    public function testSend(): void {
        $mockTokenUser = $this->getMockBuilder(User::class)->disableOriginalConstructor()->getMock();
        $mockToken = $this->getMockBuilder(Token::class)->disableOriginalConstructor()->getMock();
        $mockToken->expects(self::any())->method('getUser')->willReturn($mockTokenUser);
        $mockTokenUser->expects(self::any())->method('getUsername')->willReturn('user1');
        $mockToken->expects(self::any())->method('getUserId')->willReturn('123');
        $mockRoom = $this->getMockBuilder(Room::class)->disableOriginalConstructor()->getMock();
        $mockRoom->expects(self::any())->method('getId')->willReturn('234');

        $this->rocketChatClient->expects(self::any())
            ->method('getToken')
            ->willReturn($mockToken);

        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $mockResponse->expects(self::once())
            ->method('getBody')
            ->willReturn(json_encode(['success' => true,
                'channel' => 'channel1']));

        $mockResponse->expects(self::once())
            ->method('getHeaderLine')
            ->with('Content-Type')
            ->willReturn('application/json');

        $this->client->expects(self::once())
            ->method('post')
            ->with('/api/v1/chat.postMessage?', ['Content-Type' => 'application/json'],json_encode(['roomId' => '234','text' => 'testmessage']))
            ->willReturn($mockResponse);

        $message = $this->api->send($mockRoom,'testmessage');
        self::assertEquals('testmessage', $message->getMessage());
        self::assertEquals('123', $message->getUserId());
    }
}