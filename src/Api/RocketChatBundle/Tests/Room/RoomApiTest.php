<?php

namespace App\Api\RocketChatBundle\Tests\Room;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\RoomApi;
use App\Api\RocketChatBundle\User\Token;
use App\Api\RocketChatBundle\User\User;
use Http\Client\Common\HttpMethodsClientInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class RoomApiTest extends TestCase
{
    private MockObject $client;
    private MockObject $rocketChatClient;
    private RoomApi $api;
    protected function setUp(): void
    {
        parent::setUp();
        $this->rocketChatClient = $this->getMockBuilder(RocketChatClientInterface::class)->getMock();
        $this->client = $this->getMockBuilder(HttpMethodsClientInterface::class)->getMock();

        $this->rocketChatClient->expects(self::any())
            ->method('getHttpClient')
            ->willReturn($this->client);

        $this->api = new RoomApi($this->rocketChatClient);
    }

    public function testAll(): void {
        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $mockResponse->expects(self::once())
            ->method('getBody')
            ->willReturn(json_encode(['success' => true,
                'update' => [
                    ['_id' => '123',
                        'name' => 'room1'],
                    ['_id' => '124',
                        'fname' => 'room2'],
                    ['_id' => '125',
                        'usernames' => ['user1','user2']]
                ]]));

        $mockResponse->expects(self::once())
            ->method('getHeaderLine')
            ->with('Content-Type')
            ->willReturn('application/json');

        $this->client->expects(self::once())
            ->method('get')
            ->with('/api/v1/rooms.get?')
            ->willReturn($mockResponse);

        $mockTokenUser = $this->getMockBuilder(User::class)->disableOriginalConstructor()->getMock();
        $mockToken = $this->getMockBuilder(Token::class)->disableOriginalConstructor()->getMock();
        $mockToken->expects(self::any())->method('getUser')->willReturn($mockTokenUser);
        $mockTokenUser->expects(self::any())->method('getUsername')->willReturn('user1');

        $this->rocketChatClient->expects(self::any())
            ->method('getToken')
            ->willReturn($mockToken);

        $roomCollection = $this->api->all();

        $roomIterator = $roomCollection->getIterator();
        $room = $roomIterator->current();
        self::assertEquals('123', $room->getId());
        self::assertEquals('room1', $room->getName());

        $room = $roomIterator->next();
        self::assertEquals('124', $room?->getId());
        self::assertEquals('room2', $room?->getName());

        $room = $roomIterator->next();
        self::assertEquals('125', $room?->getId());
        self::assertEquals('user2', $room?->getName());
    }

    public function testFind(): void {
        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $mockResponse->expects(self::once())
            ->method('getBody')
            ->willReturn(json_encode(['success' => true,
                'update' => [
                    ['_id' => '123',
                        'name' => 'room1'],
                    ['_id' => '124',
                        'fname' => 'room2'],
                    ['_id' => '125',
                        'usernames' => ['user1','user2']]
                ]]));

        $mockResponse->expects(self::once())
            ->method('getHeaderLine')
            ->with('Content-Type')
            ->willReturn('application/json');

        $this->client->expects(self::once())
            ->method('get')
            ->with('/api/v1/rooms.get?')
            ->willReturn($mockResponse);

        $mockTokenUser = $this->getMockBuilder(User::class)->disableOriginalConstructor()->getMock();
        $mockToken = $this->getMockBuilder(Token::class)->disableOriginalConstructor()->getMock();
        $mockToken->expects(self::any())->method('getUser')->willReturn($mockTokenUser);
        $mockTokenUser->expects(self::any())->method('getUsername')->willReturn('user1');

        $this->rocketChatClient->expects(self::any())
            ->method('getToken')
            ->willReturn($mockToken);

        $room = $this->api->find('room1');
        self::assertEquals('123', $room->getId());
        self::assertEquals('room1', $room->getName());
    }
}