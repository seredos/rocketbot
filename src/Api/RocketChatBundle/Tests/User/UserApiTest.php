<?php

namespace App\Api\RocketChatBundle\Tests\User;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\User\User;
use App\Api\RocketChatBundle\User\UserApi;
use Http\Client\Common\HttpMethodsClientInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class UserApiTest extends TestCase
{
    private MockObject $client;
    private UserApi $api;

    protected function setUp(): void
    {
        parent::setUp();
        $rocketChatClient = $this->getMockBuilder(RocketChatClientInterface::class)->getMock();
        $this->client = $this->getMockBuilder(HttpMethodsClientInterface::class)->getMock();

        $rocketChatClient->expects(self::any())
            ->method('getHttpClient')
            ->willReturn($this->client);

        $this->api = new UserApi($rocketChatClient);
    }

    public function testAll(): void {
        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $mockResponse->expects(self::once())
            ->method('getBody')
            ->willReturn(json_encode(['success' => true,
                'users' => [
                    ['_id' => '123',
                        'name' => 'user.name',
                        'active' => false,
                        'status' => 'offline',
                        'nickname' => 'test',
                        'nameInsensitive' => '1'],
                    ['_id' => '124',
                        'name' => 'test.name',
                        'active' => true,
                        'status' => 'online',
                        'username' => 'usrname',
                        'nameInsensitive' => '2']
                ]]));

        $mockResponse->expects(self::once())
            ->method('getHeaderLine')
            ->with('Content-Type')
            ->willReturn('application/json');

        $this->client->expects(self::once())
            ->method('get')
            ->with('/api/v1/users.list?')
            ->willReturn($mockResponse);

        $userCollection = $this->api->all();

        $userIterator = $userCollection->getIterator();
        $user = $userIterator->current();
        self::assertEquals('123', $user?->getId());
        self::assertEquals('user.name', $user?->getName());
        self::assertEquals(false, $user?->isActive());
        self::assertEquals('offline', $user?->getStatus());
        self::assertEquals('test', $user?->getUsername());

        $user = $userIterator->next();

        self::assertEquals('124', $user?->getId());
        self::assertEquals('test.name', $user?->getName());
        self::assertEquals(true, $user?->isActive());
        self::assertEquals('online', $user?->getStatus());
        self::assertEquals('usrname', $user?->getUsername());
    }

    public function testOwner(): void {
        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $mockResponse->expects(self::once())
            ->method('getBody')
            ->willReturn(json_encode(['success' => true,
                '_id' => '123',
                'name' => 'user.name',
                'active' => false,
                'status' => 'offline',
                'nickname' => 'test']));

        $mockResponse->expects(self::once())
            ->method('getHeaderLine')
            ->with('Content-Type')
            ->willReturn('application/json');

        $this->client->expects(self::once())
            ->method('get')
            ->with('/api/v1/me?')
            ->willReturn($mockResponse);

        $user = $this->api->owner();
        self::assertInstanceOf(User::class, $user);
        self::assertEquals('123', $user->getId());
        self::assertEquals('user.name', $user->getName());
        self::assertEquals(false, $user->isActive());
        self::assertEquals('offline', $user->getStatus());
        self::assertEquals('test', $user->getUsername());
    }
}