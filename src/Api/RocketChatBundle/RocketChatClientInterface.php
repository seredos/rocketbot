<?php

namespace App\Api\RocketChatBundle;

use App\Api\Base\ClientInterface;
use App\Api\RocketChatBundle\Message\MessageApi;
use App\Api\RocketChatBundle\Room\RoomApi;
use App\Api\RocketChatBundle\User\Token;
use App\Api\RocketChatBundle\User\UserApi;

interface RocketChatClientInterface extends ClientInterface
{
    public function login(string $username, string $password): void;

    public function users(): UserApi;

    public function rooms(): RoomApi;

    public function messages(): MessageApi;

    public function getToken(): ?Token;
}