<?php

namespace App\Api\RocketChatBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface#
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('api_rocket_chat');
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('url')->isRequired()->end()
                ->scalarNode('user')->isRequired()->end()
                ->scalarNode('password')->isRequired()->end()
            ->end();
        return $treeBuilder;
    }
}