<?php

namespace App\Api\RocketChatBundle\DependencyInjection;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\RocketChatFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class ApiRocketChatExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->register(RocketChatFactory::class, RocketChatFactory::class);

        $container->register(RocketChatClientInterface::class, RocketChatClientInterface::class)
            ->setFactory([new Reference(RocketChatFactory::class), 'createRocketChatApi'])
            ->addArgument(new Reference('monolog.logger.api_client'))
            ->addArgument($config['url'])
            ->addArgument($config['user'])
            ->addArgument($config['password'])
            ->setPublic(true);
    }
}