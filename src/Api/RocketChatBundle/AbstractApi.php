<?php

namespace App\Api\RocketChatBundle;

class AbstractApi extends \App\Api\Base\AbstractApi
{
    protected RocketChatClientInterface $rocketChatClient;
    public function __construct(RocketChatClientInterface $client) {
        parent::__construct($client);
        $this->rocketChatClient = $client;
    }
}