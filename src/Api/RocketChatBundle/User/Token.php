<?php

namespace App\Api\RocketChatBundle\User;

use App\Api\RocketChatBundle\User\User;

class Token
{
    private string $userId;
    private string $token;
    private ?User $user;

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Token
     */
    public function setUserId(string $userId): Token
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Token
     */
    public function setToken(string $token): Token
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Token
     */
    public function setUser(?User $user): Token
    {
        $this->user = $user;
        return $this;
    }

}