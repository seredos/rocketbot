<?php

namespace App\Api\RocketChatBundle\User;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class User
{
    private string $id;
    private string $username;
    private string $status;
    private bool $active;
    private string $name;
    private string $nameInsensitive;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return User
     */
    public function setId(string $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return User
     */
    public function setStatus(string $status): User
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive(bool $active): User
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameInsensitive(): string
    {
        return $this->nameInsensitive;
    }

    /**
     * @param string $nameInsensitive
     * @return User
     */
    public function setNameInsensitive(string $nameInsensitive): User
    {
        $this->nameInsensitive = $nameInsensitive;
        return $this;
    }
}