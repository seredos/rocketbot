<?php

namespace App\Api\RocketChatBundle\User;

use App\Api\RocketChatBundle\User\Collection;
use App\Api\RocketChatBundle\User\Token;
use App\Api\RocketChatBundle\User\User;
use App\Api\Base\AbstractApi;
use Http\Client\Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserApi extends AbstractApi
{
    /**
     * @throws Exception
     */
    public function login(string $username, string $password): Token {
        $response = $this->post('/api/v1/login', ['user' => $username, 'password' => $password]);
        if(!isset($response['status']) || $response['status'] !== 'success') {
            throw new UnauthorizedHttpException($response['message'] ?? 'unknown error');
        }

        $token = new Token();
        $token->setUserId($response['data']['userId'])
            ->setToken($response['data']['authToken']);
        return $token;
    }

    /**
     * @throws Exception
     */
    public function owner(): User {
        $response = $this->get('/api/v1/me');
        if($response['success'] !== true)
            throw new UnauthorizedHttpException($response['error']);

        $user = new User();
        $user->setId($response['_id'])
            ->setName($response['name'])
            ->setActive($response['active'])
            ->setStatus($response['status'])
            ->setUsername($response['username'] ?? $response['nickname']);
        return $user;
    }

    /**
     * @throws Exception
     */
    public function all(): Collection
    {
        $response = $this->get('/api/v1/users.list');
        if($response['success'] !== true)
            throw new UnauthorizedHttpException($response['message']);

        $collection = new Collection();
        foreach($response['users'] as $user) {
            $usr = new User();
            $usr->setId($user['_id'])
                ->setUsername($user['username'] ?? $user['nickname'])
                ->setStatus($user['status'])
                ->setActive($user['active'])
                ->setName($user['name'])
                ->setNameInsensitive($user['nameInsensitive']);
            $collection->addUser($usr);
        }

        return $collection;
    }
}