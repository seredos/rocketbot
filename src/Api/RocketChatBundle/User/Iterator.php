<?php

namespace App\Api\RocketChatBundle\User;

use App\Api\RocketChatBundle\User\Collection;
use App\Api\RocketChatBundle\User\User;

class Iterator implements \Iterator
{
    private Collection $collection;
    private int $position = 0;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function current(): ?User
    {
        return $this->collection->getUser($this->position);
    }

    public function next(): ?User
    {
        $this->position++;
        return $this->collection->getUser($this->position);
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return $this->collection->getUser($this->position) !== null;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}