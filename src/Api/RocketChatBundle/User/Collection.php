<?php

namespace App\Api\RocketChatBundle\User;

use App\Api\RocketChatBundle\User\Iterator;
use App\Api\RocketChatBundle\User\User;
use IteratorAggregate;
use JetBrains\PhpStorm\Pure;

/**
 * @implements IteratorAggregate<User>
 */
class Collection implements IteratorAggregate
{
    /**
     * @var User[]
     */
    private array $users = [];

    public function addUser(User $user): void {
        $this->users[] = $user;
    }

    public function getUser(int $position): ?User {
        if(isset($this->users[$position]))
            return $this->users[$position];
        return null;
    }

    public function getIterator(): Iterator
    {
        return new Iterator($this);
    }
}