<?php

namespace App\Api\RocketChatBundle;

use Psr\Log\LoggerInterface;

class RocketChatFactory
{
    public function createRocketChatApi(LoggerInterface $logger, string $url, string $username, string $password): RocketChatClientInterface {
        $api = new RocketChatClient($logger);
        $api->setUrl($url);
        $api->login($username, $password);
        return $api;
    }
}