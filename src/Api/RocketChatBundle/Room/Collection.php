<?php

namespace App\Api\RocketChatBundle\Room;

use App\Api\RocketChatBundle\Room\Iterator;
use App\Api\RocketChatBundle\Room\Room;
use IteratorAggregate;
use JetBrains\PhpStorm\Pure;

class Collection implements IteratorAggregate
{
    /**
     * @var Room[]
     */
    private array $rooms = [];

    public function addRoom(Room $room): void {
        $this->rooms[] = $room;
    }

    public function getRoom(int $position): ?Room {
        if(isset($this->rooms[$position]))
            return $this->rooms[$position];
        return null;
    }

    public function getIterator(): Iterator
    {
        return new Iterator($this);
    }
}