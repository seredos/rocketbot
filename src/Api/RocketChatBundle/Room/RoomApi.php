<?php

namespace App\Api\RocketChatBundle\Room;

use App\Api\RocketChatBundle\AbstractApi;
use Http\Client\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class RoomApi extends AbstractApi
{
    public function find(string $name): Room {
        $collection = $this->all();
        foreach ($collection as $room) {
            if($room->getName() === $name)
                return $room;
        }

        throw new NotFoundHttpException('room not found!');
    }

    public function instantMessages(): Collection {
        $response = $this->get('/api/v1/im.list');
        if ($response['success'] !== true)
            throw new UnauthorizedHttpException($response['message']);

        $collection = new Collection();
        foreach($response['ims'] as $im) {
            $newRoom = new Room();
            $newRoom->setId($im['_id']);
            $newRoom->setName($this->getRoomName($im) ?? '');
            $collection->addRoom($newRoom);
        }
        return $collection;
    }

    /**
     * @param array<string,string|null|array<string>> $room
     * @return string|null
     */
    private function getRoomName(array $room): ?string {
        $name = $room['fname'] ?? $room['name'] ?? null;
        if($name === null && is_array($room['usernames'])) {
            $usernames = array_diff($room['usernames'], [$this->rocketChatClient->getToken()?->getUser()?->getUsername()]);
            $name = implode(', ', $usernames);
        }
        return is_string($name) ? $name : null;
    }
    
    /**
     * @throws Exception
     */
    public function all(): Collection
    {
        $response = $this->get('/api/v1/rooms.get');
        if ($response['success'] !== true)
            throw new UnauthorizedHttpException($response['message']);

        $collection = new Collection();

        foreach ($response['update'] as $room) {
            $newRoom = new Room();
            $name = $this->getRoomName($room);

            if(!$name)
                $name = $this->rocketChatClient->getToken()?->getUser()?->getUsername();

            $newRoom->setId($room['_id'])
                ->setName($name ?? '');
            $collection->addRoom($newRoom);
        }

        return $collection;
    }
}