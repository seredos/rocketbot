<?php

namespace App\Api\RocketChatBundle\Room;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Room
{
    private string $id;
    private string $name;
    private ?string $parent;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Room
     */
    public function setId(string $id): Room
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Room
     */
    public function setName(string $name): Room
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParent(): ?string
    {
        return $this->parent;
    }

    /**
     * @param string|null $parent
     * @return Room
     */
    public function setParent(?string $parent): Room
    {
        $this->parent = $parent;
        return $this;
    }
}