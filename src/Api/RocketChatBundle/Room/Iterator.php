<?php

namespace App\Api\RocketChatBundle\Room;

use App\Api\RocketChatBundle\Room\Collection;
use App\Api\RocketChatBundle\Room\Room;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Iterator implements \Iterator
{
    private Collection $collection;
    private int $position = 0;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function current(): Room
    {
        $room = $this->collection->getRoom($this->position);
        if(!$room)
            throw new NotFoundHttpException('room not found');
        return $room;
    }

    public function next(): ?Room
    {
        $this->position++;
        return $this->collection->getRoom($this->position);
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return $this->collection->getRoom($this->position) !== null;
    }

    public function rewind()
    {
        $this->position = 0;
    }
}