<?php

namespace App\Api\RocketChatBundle\Message;

class Reaction
{
    private string $type;
    /**
     * @var array<string>
     */
    private array $usernames;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Reaction
     */
    public function setType(string $type): Reaction
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getUsernames(): array
    {
        return $this->usernames;
    }

    /**
     * @param string[] $usernames
     * @return Reaction
     */
    public function setUsernames(array $usernames): Reaction
    {
        $this->usernames = $usernames;
        return $this;
    }
}