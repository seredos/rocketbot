<?php

namespace App\Api\RocketChatBundle\Message;

use App\Api\RocketChatBundle\AbstractApi;
use App\Api\RocketChatBundle\Room\Room;
use Http\Client\Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class MessageApi extends AbstractApi
{
    /**
     * @throws Exception
     */
    public function send(Room $room, string $message): Message {
        $response = $this->post('/api/v1/chat.postMessage', ['roomId' => $room->getId(), 'text' => $message]);

        $token = $this->rocketChatClient->getToken();
        if(!$token)
            throw new UnauthorizedHttpException('not authorized');

        $messageObj = new Message();
        $messageObj->setRoom($response['channel']);
        $messageObj->setMessage($message)
            ->setUserId($token->getUserId());
        return $messageObj;
    }

    /**
     * @throws Exception
     */
    public function react(Message $message, string $reaction): void {
        $this->post('/api/v1/chat.react', ['messageId' => $message->getId(), 'emoji' => $reaction, 'shouldReact' => true]);
    }

    public function imHistory(Room $room, bool $unreads, \DateTime $oldest): Collection {
        $response = $this->get('/api/v1/im.history?roomId='.
            $room->getId().
            '&oldest='.$oldest->format('Y-m-d\TH:i:s.v\Z').'&unreads='.$unreads);

        $collection = new Collection();
        if(!isset($response['messages']))
            return $collection;

        foreach ($response['messages'] as $message) {
            if(!isset($message['t']) || !in_array($message['t'],['uj','au'])) {
                $collection->addMessage($this->createMessage($message));
            }
        }
        return $collection;
    }

    /**
     * @throws Exception
     * @SuppressWarnings("boolean")
     */
    public function history(Room $room, bool $unreads = false, int $offset = 0, int $count = 10): Collection {
        $response = $this->get('/api/v1/groups.history?roomId='.$room->getId().'&offset='.$offset.'&count='.$count.'&unreads='.$unreads);

        if(!$response['success'] && $response['errorType'] === 'error-room-not-found') {
            $response = $this->get('/api/v1/channels.history?roomId='.$room->getId().'&offset='.$offset.'&count='.$count.'&unreads='.$unreads);
        }

        if(!$response['success'] && $response['errorType'] === 'error-room-not-found') {
            $response = $this->get('/api/v1/im.history?roomId='.$room->getId().'&offset='.$offset.'&count='.$count.'&unreads='.$unreads);
        }

//        if(!$response['success'] && $response['errorType'] === 'error-room-not-found') {
//            $response = $this->get('/api/v1/im.history?roomId='.$room->getId().'&offset='.$offset.'&count='.$count.'&unreads='.$unreads);
//        }

        $collection = new Collection();
        if(!isset($response['messages']))
            return $collection;

        foreach ($response['messages'] as $message) {
            if(!isset($message['t']) || !in_array($message['t'],['uj','au'])) {
                $collection->addMessage($this->createMessage($message));
            }
        }
        return $collection;
    }

    /**
     * @param array<string,mixed> $message
     * @return Message
     */
    private function createMessage(array $message): Message {
        $messageObj = new Message();
        $messageObj->setMessage($message['msg'])
            ->setId($message['_id'])
            ->setUserId($message['u']['_id']);

        if(isset($message['reactions'])) {
            foreach ($message['reactions'] as $type => $data) {
                $reaction = new Reaction();
                $reaction->setType($type)->setUsernames($data['usernames']);
                $messageObj->addReaction($reaction);
            }
        }

        return $messageObj;
    }
}