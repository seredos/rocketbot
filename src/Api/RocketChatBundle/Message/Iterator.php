<?php

namespace App\Api\RocketChatBundle\Message;

use App\Api\RocketChatBundle\Message\Collection;
use App\Api\RocketChatBundle\Message\Message;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Iterator implements \Iterator
{
    private int $position = 0;
    private Collection $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function current(): Message
    {
        $message = $this->collection->getMessage($this->position);
        if(!$message)
            throw new NotFoundHttpException('message not found');
        return $message;
    }

    public function next(): ?Message
    {
        $this->position++;
        return $this->collection->getMessage($this->position);
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return $this->collection->getMessage($this->position) !== null;
    }

    public function rewind()
    {
        $this->position = 0;
    }
}