<?php

namespace App\Api\RocketChatBundle\Message;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Message
{
    private string $id;
    private string $userId;
    private string $room;
    private string $message;
    /**
     * @var array<Reaction>
     */
    private array $reactions = [];

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @param string $room
     * @return Message
     */
    public function setRoom(string $room): Message
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Message
     */
    public function setMessage(string $message): Message
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Message
     */
    public function setId(string $id): Message
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Message
     */
    public function setUserId(string $userId): Message
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return Reaction[]
     */
    public function getReactions(): array
    {
        return $this->reactions;
    }
    
    public function addReaction(Reaction $reaction): Message {
        $this->reactions[] = $reaction;
        return $this;
    }
}