<?php

namespace App\Api\RocketChatBundle\Message;

use App\Api\RocketChatBundle\Message\Iterator;
use App\Api\RocketChatBundle\Message\Message;
use JetBrains\PhpStorm\Pure;

class Collection implements \IteratorAggregate
{
    /**
     * @var Message[]
     */
    private array $messages = [];

    public function addMessage(Message $message): void {
        $this->messages[] = $message;
    }

    public function getMessage(int $position): ?Message {
        if(isset($this->messages[$position]))
            return $this->messages[$position];
        return null;
    }

    public function getIterator(): Iterator
    {
        return new Iterator($this);
    }
    
    public function count(): int {
        return count($this->messages);
    }
}