<?php

namespace App\Api\RocketChatBundle;

use App\Api\Base\Builder;
use App\Api\Base\Client;
use App\Api\RocketChatBundle\Message\MessageApi;
use App\Api\RocketChatBundle\Room\RoomApi;
use App\Api\RocketChatBundle\User\Token;
use App\Api\RocketChatBundle\User\UserApi;
use App\Api\RocketChatBundle\Authentication;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use JetBrains\PhpStorm\Pure;


class RocketChatClient extends Client implements RocketChatClientInterface
{
    private ?Token $token;

    public function login(string $username, string $password): void {
        $userApi = $this->users();
        $this->token = $userApi->login($username, $password);

        $this->builder->removePlugin(Authentication::class);
        $this->builder->addPlugin(new AUthentication($this->token));

        $user = $userApi->owner();
        $this->token->setUser($user);
    }

    public function users(): UserApi {
        return new UserApi($this);
    }

    public function rooms(): RoomApi {
        return new RoomApi($this);
    }

    public function messages(): MessageApi {
        return new MessageApi($this);
    }

    public function getToken(): ?Token {
        return $this->token;
    }
}