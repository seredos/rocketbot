<?php

namespace App\Api\RocketChatBundle;

use App\Api\RocketChatBundle\User\Token;
use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\RequestInterface;

class Authentication implements Plugin
{
    /**
     * @var array<string,string>
     */
    private array $headers;

    public function __construct(Token $token) {
        $this->headers = self::buildHeaders($token);
    }

    /**
     * @param Token $token
     * @return array<string, string>
     */
    private static function buildHeaders(Token $token): array {
        return ['X-Auth-Token' => $token->getToken(), 'X-User-Id' => $token->getUserId()];
    }

    /**
     * @param RequestInterface $request
     * @param callable $next
     * @param callable $first
     * @return Promise
     * @SuppressWarnings("unused")
     */
    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        foreach ($this->headers as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        return $next($request);
    }
}