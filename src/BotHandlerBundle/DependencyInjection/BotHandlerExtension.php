<?php

namespace App\BotHandlerBundle\DependencyInjection;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Service\IntentDbService;
use App\Api\HolidayBundle\HolidayClientInterface;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\TimeButlerBundle\TimeButlerClientInterface;
use App\BotHandlerBundle\Command\RocketBotCommand;
use App\BotHandlerBundle\Handler\BotAnswerHandler;
use App\BotHandlerBundle\Handler\DayExcludeHandler;
use App\BotHandlerBundle\Handler\IntentPublishHandler;
use App\BotHandlerBundle\Handler\IntentReactHandler;
use App\BotHandlerBundle\Handler\IntentStoreHandler;
use App\BotHandlerBundle\Handler\GroupHandler;
use App\BotHandlerBundle\Handler\HandlerInterface;
use App\BotHandlerBundle\Handler\HolidayHandler;
use App\BotHandlerBundle\Handler\IntervalHandler;
use App\BotHandlerBundle\Handler\SendMessageHandler;
use App\BotHandlerBundle\Handler\TimeButlerHandler;
use App\BotHandlerBundle\Handler\TimeTriggerHandler;
use App\BotHandlerBundle\Service\DateTimeService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @SuppressWarnings("cyclomatic")
 * @SuppressWarnings("coupling")
 */
class BotHandlerExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->register(DateTimeService::class, DateTimeService::class)
            ->addArgument($config['timezone'])
            ->setPublic(true);
        
        $container->autowire(RocketBotCommand::class, RocketBotCommand::class)->addTag('console.command');

        foreach ($config['handlers'] as $handler => $params) {
            $definition = $container->register($handler, $this->getTypeClass($params['type']));
            $this->setTypeArguments($params['type'], $params, $definition);
        }
        $container->setAlias(HandlerInterface::class, $config['default_handler'])
            ->setPublic(true);
    }

    /**
     * @param string $type
     * @param array<string,mixed> $params
     * @param Definition $definition
     */
    private function setTypeArguments(string $type, array $params, Definition $definition): void {
        $definition->addArgument(new Reference('monolog.logger.bot_handler'));
        switch ($type) {
            case 'group':
                $handlers = [];
                foreach($params['handlers'] as $handler) {
                    $handlers[] = new Reference($handler);
                }

                $definition->addArgument($handlers);
                break;

            case 'day_exclude':
                $definition->addArgument(new Reference(DateTimeService::class));
                $definition->addArgument($params['days']);
                break;

            case 'time_trigger':
                $definition->addArgument(new Reference(DateTimeService::class));
                $definition->addArgument($params['time']);
                break;

            case 'holiday_exclude':
                $definition->addArgument(new Reference(DateTimeService::class));
                $definition->addArgument(new Reference(HolidayClientInterface::class));
                $definition->addArgument($params['countryCode']);
                break;
                
            case 'time_butler':
                $definition->addArgument(new Reference(TimeButlerClientInterface::class));
                $definition->addArgument($params['filter']);
                $definition->addArgument($params['invert']);
                break;

            case 'send_message':
                $definition->addArgument(new Reference(RocketChatClientInterface::class));
                $definition->addArgument($params['room']);
                $definition->addArgument($params['message']);
                break;

            case 'interval':
                $definition->addArgument(new Reference(DateTimeService::class));
                $definition->addArgument($params['interval']);
                break;

            case 'intent_store':
                $definition->addArgument(new Reference(RocketChatClientInterface::class));
                $definition->addArgument(new Reference(IntentDbService::class));
                $definition->addArgument($params['room']);
                $definition->addArgument($params['context']);
                break;

            case 'intent_publish':
                $definition->addArgument(new Reference(IntentDbService::class));
                $definition->addArgument(new Reference(DialogflowClient::class));
                $definition->addArgument($params['context']);
                break;
                
            case 'intent_react':
                $definition->addArgument(new Reference(RocketChatClientInterface::class));
                $definition->addArgument(new Reference(DialogflowClient::class));
                $definition->addArgument($params['room']);
                $definition->addArgument($params['context']);
                break;

            case 'bot_answer':
                $definition->addArgument(new Reference(RocketChatClientInterface::class));
                $definition->addArgument($params['message']);
                break;
        }
        if(isset($params['next']) && $params['next'] !== null)
            $definition->addArgument(new Reference($params['next']));
    }

    private function getTypeClass(string $type): string {
        switch ($type) {
            case 'group':
                return GroupHandler::class;
            case 'day_exclude':
                return DayExcludeHandler::class;
            case 'time_trigger':
                return TimeTriggerHandler::class;
            case 'holiday_exclude':
                return HolidayHandler::class;
            case 'time_butler':
                return TimeButlerHandler::class;
            case 'send_message':
                return SendMessageHandler::class;
            case 'interval':
                return IntervalHandler::class;
            case 'intent_store':
                return IntentStoreHandler::class;
            case 'intent_publish':
                return IntentPublishHandler::class;
            case 'intent_react':
                return IntentReactHandler::class;
            case 'bot_answer':
                return BotAnswerHandler::class;
        }
        return $type;
    }
}