<?php

namespace App\BotHandlerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('bot_handler');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('default_handler')->isRequired()->info('the first executed handler name in list')->end()
                ->scalarNode('timezone')->defaultValue('Europe/Berlin')->end()
                ->arrayNode('handlers')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('type')->isRequired()->info('the handler type (see documentation)')->end()
                            ->scalarNode('next')->info('the next called handler name')->end()
                            ->arrayNode('days')->scalarPrototype()->end()->end()
                            ->arrayNode('handlers')->scalarPrototype()->end()->end()
                            ->scalarNode('message')->end()
                            ->scalarNode('time')->end()
                            ->scalarNode('room')->end()
                            ->scalarNode('countryCode')->end()
                            ->scalarNode('filter')->end()
                            ->scalarNode('interval')->end()
                            ->scalarNode('context')->end()
                            ->booleanNode('invert')->defaultValue(false)->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}