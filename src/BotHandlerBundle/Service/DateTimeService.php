<?php

namespace App\BotHandlerBundle\Service;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;

class DateTimeService
{
    private DateTimeZone $timeZone;
    public function __construct(string $timezone) {
        $this->timeZone = new DateTimeZone($timezone);
    }

    /**
     * @throws Exception
     */
    public function getCurrentTimezoneOffset(): int {
        return $this->timeZone->getOffset($this->getCurrentDateTime());
    }

    /**
     * @throws Exception
     */
    public function getCurrentDateTime(): DateTime {
        return new DateTime('now', $this->timeZone);
    }

    /**
     * @throws Exception
     */
    public function createDateInterval(string $interval): DateInterval {
        return new DateInterval($interval);
    }
}