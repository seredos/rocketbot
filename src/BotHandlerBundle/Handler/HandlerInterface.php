<?php

namespace App\BotHandlerBundle\Handler;

interface HandlerInterface
{
    public function handle(): bool;
}