<?php

namespace App\BotHandlerBundle\Handler;

use App\BotHandlerBundle\Handler\AbstractHandler;
use App\BotHandlerBundle\Handler\HandlerInterface;
use Psr\Log\LoggerInterface;

class GroupHandler extends AbstractHandler
{
    /**
     * @var HandlerInterface[]
     */
    private array $handlers;
    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     * @param array<HandlerInterface> $handlers
     */
    public function __construct(LoggerInterface $logger, array $handlers)
    {
        parent::__construct();
        $this->handlers = $handlers;
        $this->logger = $logger;
    }

    protected function processing(): bool
    {
        foreach ($this->handlers as $handler) {
            $this->logger->debug('group start handler: '.get_class($handler));
            $handler->handle();
        }

        return true;
    }
}