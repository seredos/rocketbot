<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Service\IntentDbService;
use Psr\Log\LoggerInterface;

class IntentPublishHandler extends AbstractHandler
{
    private LoggerInterface $logger;
    private IntentDbService $intentService;
    private DialogflowClient $dialogflowClient;
    private ?string $context;

    public function __construct(LoggerInterface $logger, IntentDbService $intentService, DialogflowClient $dialogflowClient, ?string $context = null, ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->logger = $logger;
        $this->intentService = $intentService;
        $this->context = $context;
        $this->dialogflowClient = $dialogflowClient;
    }

    protected function processing(): bool
    {
        $this->logger->info('start publish intents');

        $contexts = [];
        if($this->context)
            $contexts[] = $this->context;

        $intents = $this->intentService->findIntents($contexts);

        $this->logger->info('delete intents');
        $this->dialogflowClient->intents(DialogflowClient::TYPE_WAIT)->delete($intents);

        foreach ($intents as $intent) {
            $this->logger->info('create intent', ['intent' => $intent->getName()]);
            $this->dialogflowClient->intents(DialogflowClient::TYPE_WAIT)->create($intent);
        }
        //$this->intentService->publishIntents($this->project, $this->context);
        $this->logger->info('intents successfully published');

        return true;
    }
}