<?php

namespace App\BotHandlerBundle\Handler;

use App\BotHandlerBundle\Service\DateTimeService;
use Psr\Log\LoggerInterface;

class DayExcludeHandler extends AbstractHandler
{
    /**
     * @var string[]
     */
    private array $excludeDays;
    private LoggerInterface $logger;
    private DateTimeService $dateTimeService;

    /**
     * @param LoggerInterface $logger
     * @param DateTimeService $dateTimeService
     * @param array<string> $excludeDays
     * @param HandlerInterface|null $successor
     */
    public function __construct(LoggerInterface $logger, DateTimeService $dateTimeService, array $excludeDays, ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->excludeDays = $excludeDays;
        $this->logger = $logger;
        $this->dateTimeService = $dateTimeService;
    }

    protected function processing(): bool
    {
        $dateTime = $this->dateTimeService->getCurrentDateTime();
        if(in_array($dateTime->format('l'), $this->excludeDays, true)) {
            $this->logger->info('current day excluded!');
            return true;
        }
        $this->logger->debug('day not excluded!');
        return false;
    }
}