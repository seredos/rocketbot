<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\DialogflowBundle\Service\IntentDbService;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use DateInterval;
use DateTime;
use Psr\Log\LoggerInterface;

class IntentStoreHandler extends AbstractHandler
{
    private static int $COUNT = 100;
    private LoggerInterface $logger;
    private RocketChatClientInterface $rocketChatClient;
    private Room $room;
    private IntentDbService $intentStoreService;
    private int $offset = 0;
    private ?string $context;
    private DateTime $refreshDate;

    public function __construct(LoggerInterface           $logger,
                                RocketChatClientInterface $rocketChatClient,
                                IntentDbService           $intentStoreService,
                                string                    $room,
                                ?string                    $context = null,
                                ?HandlerInterface         $successor = null)
    {
        parent::__construct($successor);
        $this->logger = $logger;
        $this->rocketChatClient = $rocketChatClient;
        $this->intentStoreService = $intentStoreService;
        $this->room = $rocketChatClient->rooms()->find($room);
        $this->refreshDate = new DateTime();
        $this->context = $context;
    }

    protected function processing(): bool
    {
        if((new DateTime())->getTimestamp() < $this->refreshDate->getTimestamp()) {
            return true;
        }
        $this->logger->info('intent store handler triggered');
        $history = $this->rocketChatClient->messages()
            ->history($this->room, false, $this->offset, self::$COUNT);

        $contexts = [];
        if($this->context)
            $contexts[] = $this->context;
        
        foreach ($history as $message) {
            foreach($message->getReactions() as $reaction) {
                $this->logger->debug('save intent', ['intent' => $reaction->getType(), 
                    'message' => $message->getMessage()]);
                $this->intentStoreService->saveIntent($reaction->getType(), [$message->getMessage()], $contexts);
            }
        }

        $this->offset += self::$COUNT;
        if($history->count() < self::$COUNT) {
            $this->offset = 0;
            $this->refreshDate = (new DateTime())->add(new DateInterval('P1D'));
            $this->logger->info('intents stored successfully');
            return false;
        }
        return true;
    }
}