<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use Psr\Log\LoggerInterface;

class SendMessageHandler extends AbstractHandler
{
    private LoggerInterface $logger;
    private RocketChatClientInterface $client;
    private Room $room;
    private string $message;
    public function __construct(LoggerInterface $logger, RocketChatClientInterface $client, string $room, string $message, ?HandlerInterface $successor = null)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->room = $this->client->rooms()->find($room);
        $this->message = $message;
        parent::__construct($successor);
    }

    protected function processing(): bool
    {
        $this->logger->info('send message to room '.$this->room->getName());
        $this->client->messages()->send($this->room, $this->message);
        return true;
    }
}