<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\TimeButlerBundle\TimeButlerClientInterface;
use App\Api\TimeButlerBundle\TimeButlerException;
use DateTime;
use DateTimeZone;
use Exception;
use Psr\Log\LoggerInterface;

class TimeButlerHandler extends AbstractHandler
{
    private TimeButlerClientInterface $client;
    private LoggerInterface $logger;
    private string $filter;
    private bool $invert;

    /**
     * @param LoggerInterface $logger
     * @param TimeButlerClientInterface $client
     * @param string $filter
     * @param bool $invert
     * @param HandlerInterface|null $successor
     * @SuppressWarnings("boolean")
     */
    public function __construct(LoggerInterface $logger, TimeButlerClientInterface $client, string $filter, bool $invert = false, ?HandlerInterface $successor = null)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->filter = $filter;
        $this->invert = $invert;
        parent::__construct($successor);
    }

    /**
     * @throws Exception
     */
    protected function processing(): bool
    {
        try {
            $events = $this->client->getCalendar(new DateTime('now', new DateTimeZone('Europe/Berlin')), $this->filter);
        }catch (TimeButlerException $e) {
            $this->logger->warning($e->getMessage(), ['exception' => $e]);
            return false;
        }

        $result = !count($events);
        if(!$this->invert)
            $result = (bool)count($events);

        if($result)
            $this->logger->info('time butler handler skip following handlers!', ['filter' => $this->filter]);
        else
            $this->logger->debug('time butler handler does nothing!', ['filter' => $this->filter]);

        return $result;
    }
}