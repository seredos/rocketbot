<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\HolidayBundle\HolidayClientInterface;
use App\BotHandlerBundle\Service\DateTimeService;
use Psr\Log\LoggerInterface;

class HolidayHandler extends AbstractHandler
{
    private HolidayClientInterface $client;
    private LoggerInterface $logger;
    private DateTimeService $dateTimeService;
    private string $countryCode;

    public function __construct(LoggerInterface $logger,
                                DateTimeService $dateTimeService,
                                HolidayClientInterface $client,
                                string $countryCode,
                                ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->client = $client;
        $this->logger = $logger;
        $this->dateTimeService = $dateTimeService;
        $this->countryCode = $countryCode;
    }

    protected function processing(): bool
    {
        $countryCode1 = explode('-', $this->countryCode)[0];
        $boolIsHoliday = $this->client->api()
            ->isHoliday($countryCode1,
                $this->countryCode,
                $this->dateTimeService->getCurrentTimezoneOffset());

        if ($boolIsHoliday) {
            $this->logger->info('handlers skipped. day is an holiday');
        }

        return $boolIsHoliday;
    }
}