<?php

namespace App\BotHandlerBundle\Handler;


abstract class AbstractHandler implements HandlerInterface
{
    private ?HandlerInterface $successor;

    public function __construct(?HandlerInterface $successor = null)
    {
        $this->successor = $successor;
    }

    public function handle(): bool {
        $processed = $this->processing();

        if ($processed !== true && $this->successor !== null) {
            $processed = $this->successor->handle();
        }

        return $processed;
    }

    abstract protected function processing(): bool;
}