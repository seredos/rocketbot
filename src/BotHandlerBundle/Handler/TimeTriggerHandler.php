<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use App\BotHandlerBundle\Service\DateTimeService;
use Exception as DateException;
use Http\Client\Exception;
use JetBrains\PhpStorm\Pure;
use Psr\Log\LoggerInterface;

class TimeTriggerHandler extends AbstractHandler
{
    private int $hour;
    private int $minute;
    private bool $active = false;
    private LoggerInterface $logger;
    private DateTimeService $dateTimeService;

    public function __construct(LoggerInterface $logger, DateTimeService $dateTimeService, string $time, ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->dateTimeService = $dateTimeService;
        $this->logger = $logger;
        $timeArr = explode(':', $time);
        $this->hour = (int)$timeArr[0];
        $this->minute = (int)$timeArr[1];
    }

    /**
     * @return bool
     * @SuppressWarnings("else")
     * @throws DateException
     */
    protected function processing(): bool
    {
        $dateTime = $this->dateTimeService->getCurrentDateTime();

        $currentHour = (int)$dateTime->format('H');
        $currentMinute = (int)$dateTime->format('i');

        $this->logger->debug('current time: ' . $dateTime->format('H:i'));
        $this->logger->debug('check time: ' . $this->hour . ':' . $this->minute);

        if($currentHour === $this->hour && $currentMinute === $this->minute) {
            if(!$this->active) {
                $this->active = true;
                $this->logger->info('time trigger handler executed: ', ['dateTime', $dateTime]);
                return false;
            }
        } else {
            $this->active = false;
        }

        return true;
    }
}