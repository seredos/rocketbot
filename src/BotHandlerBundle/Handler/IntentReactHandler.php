<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Service\IntentDbService;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use Psr\Log\LoggerInterface;

class IntentReactHandler extends AbstractHandler
{
    private LoggerInterface $logger;
    private RocketChatClientInterface $client;
    private DialogflowClient $dialogflowClient;
    private Room $room;
    private ?string $lastMessageId;
    private ?string $context;
    public function __construct(LoggerInterface $logger, RocketChatClientInterface $client, DialogflowClient $dialogflowClient, string $room, ?string $context = null, ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->logger = $logger;
        $this->client = $client;
        $this->dialogflowClient = $dialogflowClient;
        $this->room = $this->client->rooms()->find($room);
        $history = $this->client->messages()->history($this->room, true, 0,1);
        $this->context = $context;
        if($history->count())
            $this->lastMessageId = $history->getMessage(0)?->getId();
    }

    protected function processing(): bool
    {
        $this->logger->info('start intent interaction handler');
        $history = $this->client->messages()->history($this->room, true, 0, 20);
        
        foreach ($history as $message) {
            if($message->getId() === $this->lastMessageId) {
                $this->logger->info('message already processed. - skip handler');
                break;
            }

            //$context = $this->context ? $this->dialogflowClient->contexts()->find($this->context) : null;
            $intent = $this->dialogflowClient->intents()->detect($message->getMessage(), $this->context);
            $reaction = $intent?->getName();
            //$reaction = $this->intentService->findIntent($this->project, $message->getMessage(), $this->context);
            if($reaction && $reaction !== '') {
                $this->logger->info('react for message', ['message' => $message, 'reaction' => $reaction]);
                $this->client->messages()->react($message, $reaction);
            }
        }

        if($history->count() > 0)
            $this->lastMessageId = $history->getMessage(0)?->getId();
        return false;
    }
}