<?php

namespace App\BotHandlerBundle\Handler;

use App\BotHandlerBundle\Service\DateTimeService;
use Psr\Log\LoggerInterface;

class IntervalHandler extends AbstractHandler
{
    private LoggerInterface $logger;
    private DateTimeService $dateTimeService;
    private \DateInterval $interval;
    private \DateTime $lastDateTime;

    public function __construct(LoggerInterface$logger, DateTimeService $dateTimeService, string $interval, ?HandlerInterface $successor = null)
    {
        $this->logger = $logger;
        $this->dateTimeService = $dateTimeService;
        $this->interval = $dateTimeService->createDateInterval($interval);
        $this->lastDateTime = $dateTimeService->getCurrentDateTime();
        parent::__construct($successor);
    }

    protected function processing(): bool
    {
        $currentDateTime = $this->dateTimeService->getCurrentDateTime();
        if($currentDateTime->getTimestamp() > $this->lastDateTime->getTimestamp()) {
            $this->lastDateTime = $currentDateTime->add($this->interval);
            $this->logger->info('interval handler triggered!');
            return false;
        }
        return true;
    }
}