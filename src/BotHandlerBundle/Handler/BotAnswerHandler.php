<?php

namespace App\BotHandlerBundle\Handler;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use DateTime;
use Psr\Log\LoggerInterface;

class BotAnswerHandler extends AbstractHandler
{
    private LoggerInterface $logger;
    private RocketChatClientInterface $rocketChatClient;
    private DateTime $oldest;
    private string $message;
    public function __construct(LoggerInterface $logger,
                                RocketChatClientInterface $rocketChatClient,
                                string $message,
                                ?HandlerInterface $successor = null)
    {
        parent::__construct($successor);
        $this->logger = $logger;
        $this->rocketChatClient = $rocketChatClient;
        $this->oldest = new DateTime();
        $this->message = $message;
    }

    protected function processing(): bool
    {
        if($this->oldest->format('d') !== (new DateTime())->format('d')) {
            $this->oldest = new DateTime();
            return false;
        }
        
        $this->logger->info('checking for automated answer trigger');
        $rooms = $this->rocketChatClient->rooms()->instantMessages();
        foreach ($rooms as $room) {
            $messages = $this->rocketChatClient->messages()->imHistory($room, true, $this->oldest);
            if($messages->count() > 0) {
                $this->logger->info('send automated message', ['message' => $this->message, 'room' => $room->getName()]);
                $this->rocketChatClient->messages()->send($room, $this->message);
            }
        }
        $this->oldest = new DateTime();
        return true;
    }
}