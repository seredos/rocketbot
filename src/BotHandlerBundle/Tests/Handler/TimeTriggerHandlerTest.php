<?php

namespace App\BotHandlerBundle\Tests\Handler;

use App\Api\RocketChatBundle\Message\MessageApi;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\Api\RocketChatBundle\Room\Room;
use App\Api\RocketChatBundle\Room\RoomApi;
use App\BotHandlerBundle\Handler\HandlerInterface;
use App\BotHandlerBundle\Handler\TimeTriggerHandler;
use App\BotHandlerBundle\Service\DateTimeService;
use DateTime;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class TimeTriggerHandlerTest extends TestCase
{
    public function testNotTriggered(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $mockDateTimService = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();
        $mockHandler = $this->getMockBuilder(HandlerInterface::class)->getMock();

        $handler = new TimeTriggerHandler($mockLogger,
            $mockDateTimService,
            '10:00',
            $mockHandler);

        $mockDateTimService->expects($this->once())
            ->method('getCurrentDateTime')
            ->willReturn(new DateTime('2021-10-10 10:01'));

        $this->assertEquals(true, $handler->handle());
    }

    public function testTriggered(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $mockDateTimService = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();
        $mockHandler = $this->getMockBuilder(HandlerInterface::class)->getMock();

        $handler = new TimeTriggerHandler($mockLogger,
            $mockDateTimService,
            '10:00',
            $mockHandler);

        $mockDateTimService->expects($this->once())
            ->method('getCurrentDateTime')
            ->willReturn(new DateTime('2021-10-10 10:00'));

        $this->assertEquals(false, $handler->handle());
    }
}