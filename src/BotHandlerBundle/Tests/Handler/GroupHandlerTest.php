<?php

namespace App\BotHandlerBundle\Tests\Handler;

use App\BotHandlerBundle\Handler\GroupHandler;
use App\BotHandlerBundle\Handler\HandlerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class GroupHandlerTest extends TestCase
{
    public function testGroupHandler(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();

        $mockHandler1 = $this->getMockBuilder(HandlerInterface::class)->getMock();
        $mockHandler2 = $this->getMockBuilder(HandlerInterface::class)->getMock();

        $groupHandler = new GroupHandler($mockLogger, [$mockHandler1, $mockHandler2]);

        $mockHandler1->expects(self::once())->method('handle');
        $mockHandler2->expects(self::once())->method('handle');

        self::assertEquals(true, $groupHandler->handle());
    }
}