<?php

namespace App\BotHandlerBundle\Tests\Handler;

use App\BotHandlerBundle\Handler\DayExcludeHandler;
use App\BotHandlerBundle\Handler\HandlerInterface;
use App\BotHandlerBundle\Service\DateTimeService;
use DateTime;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class DayExcludeHandlerTest extends TestCase
{
    public function testNotExcluded(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $mockDateTimService = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();
        $mockHandler = $this->getMockBuilder(HandlerInterface::class)->getMock();

        $handler = new DayExcludeHandler($mockLogger, $mockDateTimService, ['Sunday', 'Saturday'], $mockHandler);

        $mockDateTimService->expects($this->once())
            ->method('getCurrentDateTime')
            ->willReturn(new DateTime('2021-10-11 10:00'));

        $mockHandler->expects($this->once())->method('handle');

        $this->assertEquals(false, $handler->handle());
    }

    public function testExcluded(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $mockDateTimService = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();
        $mockHandler = $this->getMockBuilder(HandlerInterface::class)->getMock();

        $handler = new DayExcludeHandler($mockLogger, $mockDateTimService, ['Sunday', 'Saturday'], $mockHandler);

        $mockDateTimService->expects($this->once())
            ->method('getCurrentDateTime')
            ->willReturn(new DateTime('2021-10-10 10:00'));

        $mockHandler->expects($this->never())->method('handle');

        $this->assertEquals(true, $handler->handle());
    }
}