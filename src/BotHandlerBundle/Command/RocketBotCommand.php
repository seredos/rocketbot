<?php

namespace App\BotHandlerBundle\Command;

use App\BotHandlerBundle\Handler\HandlerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'rocket:bot',
    description: 'Add a short description for your command',
)]
class RocketBotCommand extends Command
{
    private HandlerInterface $handler;
    public function __construct(HandlerInterface $handler, string $name = null)
    {
        $this->handler = $handler;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption('interval', null, InputOption::VALUE_REQUIRED, 'the tick interval', 1)
        ;
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $end = 0;
        do {
            $start = time();
            if($start - $end > $input->getOption('interval')) {
                $memUsage = memory_get_usage(true);

                $output->writeln('execute handlers - '.round($memUsage/1024,2)." kilobytes");
                $this->handler->handle();
                $end = time();
            }
        }while($input->getOption('interval') > 0);

        return Command::SUCCESS;
    }
}
