<?php

namespace App\Tests\Api\Base;

use App\Api\Base\Builder;
use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\Plugin;
use Http\Client\Common\PluginClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

class BuilderTest extends TestCase
{
    private MockObject $client;
    private MockObject $requestFactory;
    private MockObject $streamFactory;
    private MockObject $uriFactory;
    private Builder $builder;

    protected function setUp(): void
    {
        $this->client = $this->getMockBuilder(ClientInterface::class)->getMock();
        $this->requestFactory = $this->getMockBuilder(RequestFactoryInterface::class)->getMock();
        $this->streamFactory = $this->getMockBuilder(StreamFactoryInterface::class)->getMock();
        $this->uriFactory = $this->getMockBuilder(UriFactoryInterface::class)->getMock();

        $this->builder = new Builder($this->client, $this->requestFactory, $this->streamFactory, $this->uriFactory);
    }

    public function testAddRemovePlugin(): void {
        $httpClient1 = $this->builder->getHttpClient();

        self::assertInstanceOf(HttpMethodsClient::class, $httpClient1);

        $httpClient2 = $this->builder->getHttpClient();

        self::assertEquals($httpClient1, $httpClient2);

        $mockPlugin = $this->getMockBuilder(Plugin::class)->getMock();

        $this->builder->addPlugin($mockPlugin);

        $httpClient3 = $this->builder->getHttpClient();

        self::assertNotEquals($httpClient2, $httpClient3);

        $this->builder->removePlugin(get_class($mockPlugin));

        $httpClient4 = $this->builder->getHttpClient();
        self::assertNotEquals($httpClient3, $httpClient4);
    }
}