<?php

namespace App\Tests\Api\Base;

use App\Api\Base\QueryStringBuilder;
use PHPUnit\Framework\TestCase;

class QueryStringBuilderTest extends TestCase
{
    public function testBuild(): void {
        $query = QueryStringBuilder::build(['a' => 'b', 'c' => 'd']);

        self::assertEquals('?a=b&c=d',$query);
    }

    public function testBuildWithArrayValue(): void {
        $query = QueryStringBuilder::build(['a' => ['b','c','d']]);

        self::assertEquals('?a%5B%5D=b&a%5B%5D=c&a%5B%5D=d',$query);
    }
}