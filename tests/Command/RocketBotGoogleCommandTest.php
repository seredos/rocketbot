<?php

namespace App\Tests\Command;

use App\Api\DialogflowBundle\DialogflowClient;
use App\Api\DialogflowBundle\Entity\Intent;
use App\Api\DialogflowBundle\Service\IntentDbService;
use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\BotHandlerBundle\Handler\HandlerInterface;
use App\BotHandlerBundle\Handler\IntentPublishHandler;
use App\BotHandlerBundle\Handler\IntentStoreHandler;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RocketBotGoogleCommandTest extends KernelTestCase
{
    public function testSoreIntent(): void {
        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();

        $kernel = static::createKernel();
        $kernel->boot();

        $client = $kernel->getContainer()->get(RocketChatClientInterface::class);
        $dbService = $kernel->getContainer()->get(IntentDbService::class);
        $kernel->getContainer()->set(HandlerInterface::class,
            new IntentStoreHandler($mockLogger,
                $client,
                $dbService,
                'general',
                'emoji_react'));

        $kernel->getContainer()->get('doctrine.orm.entity_manager')
            ->createQuery('DELETE FROM '.Intent::class)
            ->execute();

        $application = new Application($kernel);

        $command = $application->find('rocket:bot');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--interval' => -1
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('execute handlers', $output);

        $intents = $kernel->getContainer()->get('doctrine.orm.entity_manager')->createQueryBuilder()->select('i')
            ->from(Intent::class,'i')->getQuery()->getResult();
        self::assertTrue(count($intents) > 0);
        foreach ($intents as $intent) {
            self::assertEquals('emoji_react', $intent->getContexts()[0]->getName());
        }
    }

    public function testPublishIntent(): void {
        $kernel = static::createKernel();
        $kernel->boot();

        $mockLogger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $dbService = $kernel->getContainer()->get(IntentDbService::class);
        $dialogflowClient = $kernel->getContainer()->get(DialogflowClient::class);

        $kernel->getContainer()->set(HandlerInterface::class,
            new IntentPublishHandler($mockLogger, $dbService, $dialogflowClient,'emoji_react'));

        $mockLogger->expects(self::at(0))->method('info')->with('start publish intents');
        $mockLogger->expects(self::at(1))->method('info')->with('delete intents');
        $mockLogger->expects(self::at(2))->method('info')->with('create intent');

        $application = new Application($kernel);

        $command = $application->find('rocket:bot');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--interval' => -1,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('execute handlers', $output);
    }
}