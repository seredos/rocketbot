<?php

namespace App\Tests\Command;

use App\Api\RocketChatBundle\RocketChatClientInterface;
use App\BotHandlerBundle\Service\DateTimeService;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RocketBotCommandTest extends KernelTestCase
{
    public function testExecuteDayExcluded(): void {
        $kernel = static::createKernel();
        $kernel->boot();
        $application = new Application($kernel);

        $client = $kernel->getContainer()->get(RocketChatClientInterface::class);
        $room = $client->rooms()->find('general');
        $client->messages()->send($room, 'test');

        $dateTimeServiceMock = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();

        $dateTimeServiceMock->expects(self::any())
            ->method('getCurrentDateTime')
            ->willReturn(new \DateTime('2021-08-15 10:00'));

        $kernel->getContainer()->set(DateTimeService::class, $dateTimeServiceMock);
        $command = $application->find('rocket:bot');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--interval' => -1
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('execute handlers', $output);

        $messages = $client->messages()->history($room);

        $this->assertEquals('test', $messages->getMessage(0)?->getMessage());
    }

    public function testExecuteMorning(): void {
        $kernel = static::createKernel();
        $kernel->boot();
        $application = new Application($kernel);

        $dateTimeServiceMock = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();

        $dateTimeServiceMock->expects(self::any())
            ->method('getCurrentDateTime')
            ->willReturn(new \DateTime('2021-10-11 10:00'));

        $kernel->getContainer()->set(DateTimeService::class, $dateTimeServiceMock);
        $command = $application->find('rocket:bot');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--interval' => -1
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('execute handlers', $output);

        $client = $kernel->getContainer()->get(RocketChatClientInterface::class);
        $room = $client->rooms()->find('general');
        $messages = $client->messages()->history($room);

        $this->assertEquals('morgen', $messages->getMessage(0)?->getMessage());
    }

    public function testExecuteBye(): void {
        $kernel = static::createKernel();
        $kernel->boot();
        $application = new Application($kernel);

        $dateTimeServiceMock = $this->getMockBuilder(DateTimeService::class)->disableOriginalConstructor()->getMock();

        $dateTimeServiceMock->expects(self::any())
            ->method('getCurrentDateTime')
            ->willReturn(new \DateTime('2021-10-11 19:00'));

        $kernel->getContainer()->set(DateTimeService::class, $dateTimeServiceMock);
        $command = $application->find('rocket:bot');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--interval' => -1
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('execute handlers', $output);

        $client = $kernel->getContainer()->get(RocketChatClientInterface::class);
        $room = $client->rooms()->find('general');
        $messages = $client->messages()->history($room);

        $this->assertEquals('bye', $messages->getMessage(0)?->getMessage());
    }
}